package protocols;

import application.Application;
import common.*;
import configs.NetworkConfig;
import helpers.DatabaseHelper;
import helpers.ProtobufMessageHelper;
import protobuf.ProtoMessage;

import java.util.ArrayList;
import java.util.List;

/*Protocol for propagating Resource consumption data between nodes.
 * All nodes should have a semi-clear picture of what the resource consumption of other apps and nodes is.
 * Primary node uses the stats to create a migration plan. Stats are included in the block for plan verification.
 * */
public class ResourcePropagationProtocol {

	public static void digest(ProtoMessage.Message message, Node origin) {
		Stat[] stats = ProtobufMessageHelper.readMessageWithStats(message);
		App[] apps = ProtobufMessageHelper.readMessageWithApps(message);
		// System.out.println(Constants.RESOURCE + " Received resource stats from node "
		// + origin.getId());
		for (int i = 0; i < stats.length; i++) {
			for (int j = 0; j < stats.length; j++) {
				DatabaseHelper.addApp(apps[i].getId(), apps[i].getDid(), apps[i].getName(), stats[i].getCpu(), stats[i].getRam(), stats[i].getDisk(), stats[i].getTimestamp(), stats[i].getNode_id());
			}
		}
	}

	public static void updateResourceStats(Node node) {
		AppsWithStats appsWithStats = DatabaseHelper.getApps();
        App[] apps = appsWithStats.apps;
        Stat[] stats = appsWithStats.stats;

        List<App> finalApps = new ArrayList<>();
        List<Stat> finalStats = new ArrayList<>();

        for (int i = 0; i < apps.length; i++) {
            if (stats[i].getNode_id().equals(NetworkConfig.node_id)) {
                finalApps.add(apps[i]);
                finalStats.add(stats[i]);
            }
        }

        ProtoMessage.Message message = ProtobufMessageHelper.writeMessageWithAppsAndStats(
					ProtoMessage.Message.Protocol.RESOURCE_PROPAGATION_PROTOCOL,
					finalStats.toArray(new Stat[0]), finalApps.toArray(new App[0]));
			node.send(message);
	}

	public static void broadcastResourceStats() {
		// System.out.println(Constants.RESOURCE + " Broadcasting resource stats... ");
		for (Node node : Application.known_nodes.values()) {
			updateResourceStats(node);
		}
	}
}

package protocols;

import application.Application;
import common.Block;
import common.MessageModel;
import common.Node;
import helpers.*;
import protobuf.ProtoMessage;

public class BlockchainSyncProtocol {

	public static void digest(ProtoMessage.Message message, Node origin) {
		MessageModel messageModel = ProtobufMessageHelper.readData(message);
		ProtoMessage.Message.Payload payload = messageModel.content;
		if (messageModel.type == ProtoMessage.Message.Type.RESPONSE) {
			Block[] blocks = ProtobufMessageHelper.readBlocks(payload);
			Logger.log(Constants.SYNC + "Received  " + blocks.length + " new blocks from " + origin.getId(), Logger.Level.BLOCKCHAIN);
			BlockchainHelper.addNewBlocks(blocks);
		} else if (messageModel.type == ProtoMessage.Message.Type.REQUEST) {
			//Logger.log(Constants.SYNC + "Received request for block height. " + message + " " + messageModel, Logger.Level.BLOCKCHAIN);

			int block_height = messageModel.content.getHeight();

			if (DatabaseHelper.getAllBlocks().length > 0 && DatabaseHelper.getLastBlock().getId() > block_height) {
				// we are in the future, send a list of missing nodes
				Block[] candidates = DatabaseHelper.getBlocksFromID(block_height);
				Logger.log(Constants.SYNC + "Peer fell behind by " + candidates.length + " blocks. Sending blocks..", Logger.Level.BLOCKCHAIN);
				ProtoMessage.Message m = ProtobufMessageHelper.writeMessageWithBlocks(
					ProtoMessage.Message.Protocol.BLOCKCHAIN_SYNC_PROTOCOL, ProtoMessage.Message.Type.RESPONSE, candidates
				);
				origin.send(m);
			}
		}
	}

	public static void requestBlocks(Node node) {
		Application.chain_synced.set(false);
		Logger.log(Constants.SYNC + "Asking for current block...", Logger.Level.BLOCKCHAIN);
		int height = DatabaseHelper.getLastBlock().getId();
		ProtoMessage.Message m = ProtobufMessageHelper.writeMessageWithBlockHeight(
				ProtoMessage.Message.Protocol.BLOCKCHAIN_SYNC_PROTOCOL, ProtoMessage.Message.Type.REQUEST, height
		);
		node.send(m);
	}
}

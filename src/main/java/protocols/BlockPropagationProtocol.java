package protocols;

import application.Application;
import common.Block;
import common.MessageModel;
import common.Node;
import helpers.*;
import protobuf.ProtoMessage;
import services.Database;

public class BlockPropagationProtocol {

    public static void digest(ProtoMessage.Message message, Node origin) {
        MessageModel messageModel = ProtobufMessageHelper.readData(message);
        ProtoMessage.Message.Payload payload = messageModel.content;
        if (messageModel.type == ProtoMessage.Message.Type.NEW_BLOCK) {
            //BaseProtocolLayer.sendBroadcast(message);
            Block[] candidates = ProtobufMessageHelper.readBlocks(payload);
            Block candidate = new Block();
            if (candidates.length > 0) {
                candidate = candidates[0];
            }
            BlockchainHelper.addBlock(candidate);
            Block[] blocks = DatabaseHelper.getBlockByID(candidate.getId());
            if (blocks.length > 0) {
                if (candidate.getTicket() < blocks[0].getTicket()) {
                    GossipHelper.broadcastMessage(message, origin);
                }
            }
        }
    }

    public static void sendNewBlock(Block block, Node node) {
        Block[] blocks = DatabaseHelper.getBlocksFromID(block.getId());
        ProtoMessage.Message message = ProtobufMessageHelper.writeMessageWithBlocks(ProtoMessage.Message.Protocol.BLOCK_PROPAGATION_PROTOCOL,
                ProtoMessage.Message.Type.NEW_BLOCK, blocks);
        node.send(message);
        //BaseProtocolLayer.sendBroadcast(message);
    }

    public static void broadcastNewBlock(Block block) {
        for (Node n : Application.known_nodes.values()) {
            sendNewBlock(block, n);
        }
    }
}

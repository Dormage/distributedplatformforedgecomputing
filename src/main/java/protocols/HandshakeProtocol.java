package protocols;

import common.MessageModel;
import common.Node;
import configs.NetworkConfig;
import helpers.Constants;
import helpers.Logger;
import helpers.ProtobufMessageHelper;
import protobuf.ProtoMessage;

public class HandshakeProtocol {
	
	public static void sendHandshake(Node node) {
		String[] payload = new String[5];
		payload[0] = NetworkConfig.node_id;
		payload[1] = ""+System.currentTimeMillis();
		payload[2] = NetworkConfig.local_ip;
		payload[3] = ""+NetworkConfig.server_port;
		payload[4] = ""+node.getRandomSeed();

		ProtoMessage.Message protocolMessage = ProtobufMessageHelper.writeMessageWithStringArray(ProtoMessage.Message.Protocol.HANDSHAKE_PROTOCOL,
				ProtoMessage.Message.Type.REQUEST, payload);
		node.send(protocolMessage);
		//System.out.println(Constants.PROTOCOL +  " Sending handshake to new connection ");
	}

	public static void digest(ProtoMessage.Message message, Node node) {
		MessageModel messageModel = ProtobufMessageHelper.readData(message);
		ProtoMessage.Message.Payload payload = messageModel.content;
		if(node.getRandomSeed() == Double.parseDouble(messageModel.content.getNode(4))){
			Logger.log(Constants.NETWORK + "Accidentally connected to self. Dropping connection.", Logger.Level.NETWORK);
			node.close();
			return;
		}
		node.setId(payload.getNode(0));
		node.setTimestamp(Long.parseLong(payload.getNode(1)));
		node.setAddress(payload.getNode(2));
		node.setPort(Integer.parseInt(payload.getNode(3)));
		node.setHandshake(true);
		//System.out.println(Constants.PROTOCOL + " Received handshake from node " + node.getId());
	}
}

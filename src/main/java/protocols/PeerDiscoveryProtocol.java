package protocols;

import application.Application;
import common.MessageModel;
import common.Node;
import common.NodeModel;
import configs.NetworkConfig;
import helpers.*;
import protobuf.ProtoMessage;
import services.PeerDiscoveryThread;
import java.util.Random;

public class PeerDiscoveryProtocol {

	public static void digest(protobuf.ProtoMessage.Message message, Node node) {
		if (message.getType() == ProtoMessage.Message.Type.REQUEST) {
            NodeModel[] knownNodes = DatabaseHelper.getAllKnownNodes();
			knownNodes = shuffleArray(knownNodes);
			int i = 0;
			String[] nodes = new String[knownNodes.length];
			for (NodeModel n : knownNodes) {
				nodes[i] = n.getIp() + "," + n.getPort() + "," + n.getId();
				i++;
			}

			ProtoMessage.Message m = ProtobufMessageHelper.writeMessageWithStringArray(ProtoMessage.Message.Protocol.PEER_DISCOVERY_PROTOCOL,
					ProtoMessage.Message.Type.RESPONSE, nodes);
			node.send(m);
		} else if (message.getType() == ProtoMessage.Message.Type.RESPONSE) {
			MessageModel messageModel = ProtobufMessageHelper.readData(message);

//			int requred_new_connecions = NetworkConfig.requiered_nodes- Application.known_nodes.size();
//			if(requred_new_connecions>0){
//				int minNodes = Math.min(requred_new_connecions,messageModel.content.getNodeCount());
//				for (int i = 0; i < minNodes; i++) {
//					Application.executor.execute(new PeerDiscoveryThread(messageModel.content.getNode(i)));
//				}
//			}
			for (int i = 0; i < messageModel.content.getNodeCount(); i++) {
				Application.executor.execute(new PeerDiscoveryThread(messageModel.content.getNode(i)));
			}
		}
	}

	public static void requestAddr(Node n) {
		String[] payload = new String[1];
		payload[0] = String.valueOf(Math.random());
		ProtoMessage.Message m = ProtobufMessageHelper.writeMessageWithStringArray(ProtoMessage.Message.Protocol.PEER_DISCOVERY_PROTOCOL,
				ProtoMessage.Message.Type.REQUEST, payload);
		n.send(m);
	}
	
	public static void broadcastNewNode(Node n) {
		String[] new_node = new String[1];
		new_node[0] = n.getAddress() + "," + n.getPort() + "," + n.getId();
		ProtoMessage.Message m = ProtobufMessageHelper.writeMessageWithStringArray(ProtoMessage.Message.Protocol.PEER_DISCOVERY_PROTOCOL,
				ProtoMessage.Message.Type.RESPONSE, new_node);
		NetworkUtil.broadcastToAll(m);
	}

	private static NodeModel[] shuffleArray(NodeModel[] array)
	{
		NodeModel temp;
		int index;
		Random random = new Random();
		for (int i = array.length - 1; i > 0; i--)
		{
			index = random.nextInt(i + 1);
			temp = array[index];
			array[index] = array[i];
			array[i] = temp;
		}
		return array;
	}
}

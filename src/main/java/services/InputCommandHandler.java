package services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import application.Application;
import helpers.*;

public class InputCommandHandler extends Thread {
	BufferedReader br;
	String command;

	public InputCommandHandler() {
		br = new BufferedReader(new InputStreamReader(System.in));
	}

	public void run() {
		try {
			while ((command = br.readLine()) != null) {
				switch (command) {
				case "q": //quit
					System.exit(0); //should do clean exit in future
					break;
				case "lb":
					BlockchainHelper.printBlockChain();
					break;
				case "ln":
					NetworkUtil.printNodes();
					break;
				case "bdb":
					int start = Integer.parseInt(br.readLine());
					int end = Integer.parseInt(br.readLine());
					DockerUtil.printBlockDB(start, end);
					break;
				case "ndb":
					DockerUtil.printKnownNodesDB();
					break;
				case "adb":
					DockerUtil.printAppsWithStats();
					break;
                case "rdb":
                    DockerUtil.printRunningAppsFromDB();
                    break;
				case "nid":
					DockerUtil.printNodeID();
					break;
				case "ldb":
					int offset = Integer.parseInt(br.readLine());
					DockerUtil.printLogs(offset);
					break;
				case "start_vdf":
					Application.chain_synced.set(true);
					LotteryHelper.drawLottery(DatabaseHelper.getLastBlock());
					break;
				default:
					break;
				}
			}
		} catch (IOException e) {
			System.out.println(Constants.ERROR + "failed to read STDIN.");
			Logger.log(Constants.ERROR + "failed to read STDIN.", Logger.Level.ERROR);
		}
	}
}

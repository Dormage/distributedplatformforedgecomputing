package services;

import com.google.gson.Gson;
import common.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import helpers.Constants;
import helpers.Logger;

public class Database {

	Connection connection;
	String user = "";
	String password = "";
	
	public Database(String database) {
		Statement statement;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver" );
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	    try {
	    	connection = DriverManager.getConnection("jdbc:hsqldb:file:database/" + database + ";shutdown=true", user, password);
			statement = connection.createStatement();
			this.createTables();
	    } catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void createTables() {
		String query;
		Statement statement = null;
		try {
			statement = connection.createStatement();

			query = "CREATE TABLE IF NOT EXISTS knownNodes "
					+ "(id INTEGER IDENTITY PRIMARY KEY, node_id VARCHAR(256), ip VARCHAR(45), port SMALLINT, ping TIMESTAMP)";
			statement.execute(query);

			query = "CREATE TABLE IF NOT EXISTS blockDB "
					+ "(id INTEGER IDENTITY PRIMARY KEY, block_hash VARCHAR(256), previous_block_hash VARCHAR(256), block_id INTEGER," +
					" ticket INTEGER, difficulty INTEGER, vdf_proof VARCHAR(1033), block_producer VARCHAR(256), ts TIMESTAMP , data VARCHAR(1000000), " +
					"block_final BOOLEAN)";
			statement.execute(query);

			query = "CREATE TABLE IF NOT EXISTS appDB "
					+ "(id INTEGER IDENTITY PRIMARY KEY, app_id VARCHAR(256), did VARCHAR(256), name VARCHAR(75), " +
					"cpu DOUBLE, ram DOUBLE, disk DOUBLE, node_id VARCHAR(256), ts TIMESTAMP)";
			statement.execute(query);

			query = "CREATE TABLE IF NOT EXISTS logDB "
					+ "(id INTEGER IDENTITY PRIMARY KEY, log VARCHAR(256), ts TIMESTAMP)";
			statement.execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void deleteFromTable(String table, int id) {
		String query = String.format("DELETE FROM " + table + " WHERE id='%s'", id);
		int status;
		Statement statement = null;
		try {
			statement = connection.createStatement();
			status = statement.executeUpdate(query);
			if (status == 1) {
				//Logger.log(id + " deleted from " + table, Logger.Level.INFO);
			} else {
				Logger.log(Constants.ERROR+"Error deleting " + id + " from " + table, Logger.Level.ERROR);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void insertIntoTable(String query) {
		int status;
		Statement statement = null;
		try {
			statement = connection.createStatement();

			status = statement.executeUpdate(query);
			if (status == 1) {
				Logger.log("Successfully inserted!", Logger.Level.INFO);
			} else {
				Logger.log("Error inserting!", Logger.Level.ERROR);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void addNodeToKnownNodes(String nodeID, String ip, int port, Timestamp ping) {
		String query = "MERGE INTO knownNodes AS kn USING (VALUES(?, ?, ?, ?)) AS vals(node_id, ip, port, ping) " +
				"ON kn.node_id=vals.node_id " +
				"WHEN MATCHED THEN UPDATE SET kn.ping=vals.ping " +
				"WHEN NOT MATCHED THEN INSERT (node_id, ip, port, ping) VALUES vals.node_id, vals.ip, vals.port, vals.ping";

		PreparedStatement preparedStatement = null;

		try {
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, nodeID);
			preparedStatement.setString(2, ip);
			preparedStatement.setInt(3, port);
			preparedStatement.setTimestamp(4, ping);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public NodeModel[] getKnownNodes(String query, String title) {
		ResultSet resultSet;
		Statement statement = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			List<NodeModel> nodes = new ArrayList<>();
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String nodeID = resultSet.getString("node_id");
				String ip = resultSet.getString("ip");
				int port = resultSet.getInt("port");
				Timestamp ping = resultSet.getTimestamp("ping");
				NodeModel node = new NodeModel(nodeID, ip, port, ping);
				nodes.add(node);
			}
			return nodes.toArray(new NodeModel[0]);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public Block[] getBlocks(String query) {
		ResultSet resultSet;
		HashMap<String, Stat> data = new HashMap();
		Statement statement = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			List<Block> blocks = new ArrayList<>();
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				int blockID = resultSet.getInt("block_id");
				int ticket = resultSet.getInt("ticket");
				int difficulty = resultSet.getInt("difficulty");
				String blockHash = resultSet.getString("block_hash");
				String prevBlockHash = resultSet.getString("previous_block_hash");
				String vdfProof = resultSet.getString("vdf_proof");
				String block_producer = resultSet.getString("block_producer");
				Long timestamp = resultSet.getTimestamp("ts").getTime();
				boolean block_final = resultSet.getBoolean("block_final");

				Type type = new TypeToken<HashMap<String, Stat>>(){}.getType();

				data = new Gson().fromJson(resultSet.getString("data"), type);

				if (data.size() == 0) {
					data = new HashMap<>();
				}

				Block block = new Block(blockHash, prevBlockHash, blockID, ticket, difficulty, vdfProof, block_producer, timestamp, data, block_final);
				blocks.add(block);
			}
			return blocks.toArray(new Block[0]);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public void deleteBlockFromTable(int id) {
		String query = String.format("DELETE FROM blockDB WHERE block_id='%s'", id);
		int status;
		Statement statement = null;
		try {
			statement = connection.createStatement();
			status = statement.executeUpdate(query);
			if (status == 1) {
				Logger.log(Constants.CHAIN + "Block " + id + " deleted from blockDB", Logger.Level.INFO);
			} else {
				Logger.log(Constants.ERROR+"Error deleting block " + id + " from blockDB", Logger.Level.ERROR);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void addBlockToDB(Block block) {

		String query = "MERGE INTO blockDB AS b USING (VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)) AS vals(block_id, block_hash, previous_block_hash, ticket, difficulty, vdf_proof, block_producer, ts, data, block_final) " +
				"ON b.block_id=vals.block_id " +
				"WHEN MATCHED THEN UPDATE SET b.block_hash=vals.block_hash, b.previous_block_hash=vals.previous_block_hash, b.ticket=vals.ticket, " +
				"b.difficulty=vals.difficulty, b.vdf_proof=vals.vdf_proof, b.block_producer=vals.block_producer, " +
				"b.ts=vals.ts, b.data=vals.data, b.block_final=vals.block_final " +
				"WHEN NOT MATCHED THEN INSERT (block_id, block_hash, previous_block_hash, ticket, difficulty, vdf_proof, block_producer, ts, data, block_final) " +
				"VALUES vals.block_id, vals.block_hash, vals.previous_block_hash, vals.ticket, vals.difficulty, vals.vdf_proof, " +
				"vals.block_producer, vals.ts, vals.data, vals.block_final";

		PreparedStatement preparedStatement = null;

		try {
			if (block.data == null) {
				block.data = new HashMap<>();
			}
			String data = new Gson().toJson(block.data);
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, block.getId());
			preparedStatement.setString(2, block.getBlock_hash());
			preparedStatement.setString(3, block.getPrevious_block_hash());
			preparedStatement.setInt(4, block.getTicket());
			preparedStatement.setInt(5, block.getDifficulty());
			preparedStatement.setString(6, block.getVdf_proof());
			preparedStatement.setString(7, block.getBlock_producer());
			preparedStatement.setTimestamp(8, new Timestamp(block.getTimestamp()));
			preparedStatement.setString(9, data);
			preparedStatement.setBoolean(10, block.getBlock_final());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public AppsWithStats getApps(String query) {
		ResultSet resultSet;
		List<App> apps = new ArrayList<>();
		List<Stat> stats = new ArrayList<>();
		Statement statement = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				String appID = resultSet.getString("app_id");
				String dockerID = resultSet.getString("did");
				String name = resultSet.getString("name");
				double cpu = resultSet.getDouble("cpu");
				double ram = resultSet.getDouble("ram");
				double disk = resultSet.getDouble("disk");
				Long timestamp = resultSet.getTimestamp("ts").getTime();
				String node_id = resultSet.getString("node_id");
				App app = new App(appID, dockerID, name);
				Stat stat = new Stat(appID, cpu, ram, disk, timestamp, node_id);
				apps.add(app);
				stats.add(stat);
			}
			return new AppsWithStats(apps.toArray(new App[0]), stats.toArray(new Stat[0]));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public void addAppToDB(String appID, String dID, String name, double cpu, double ram, double disk, long ts, String nodeID) {
		String query = "MERGE INTO appDB AS a USING (VALUES(?, ?, ?, ?, ?, ?, ?, ?)) AS vals(app_id, did, name, cpu, ram, disk, ts, node_id) " +
				"ON a.app_id=vals.app_id " +
				"WHEN MATCHED THEN UPDATE SET a.did=vals.did, a.name=vals.name, a.cpu=vals.cpu, a.ram=vals.ram, a.disk=vals.disk, a.ts=vals.ts, a.node_id=vals.node_id " +
				"WHEN NOT MATCHED THEN INSERT (app_id, did, name, cpu, ram, disk, ts, node_id) VALUES vals.app_id, vals.did, vals.name, " +
				"vals.cpu, vals.ram, vals.disk, vals.ts, vals.node_id";

		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, appID);
			preparedStatement.setString(2, dID);
			preparedStatement.setString(3, name);
			preparedStatement.setDouble(4, cpu);
			preparedStatement.setDouble(5, ram);
			preparedStatement.setDouble(6, disk);
			preparedStatement.setTimestamp(7, new Timestamp(ts));
			preparedStatement.setString(8, nodeID);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void close() {
		Statement statement = null;
		try {
			statement = connection.createStatement();
			if (connection != null) {
				connection.close();
			}
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void updateApp(String appID, String dID, double cpu, double ram, double disk, long ts, String nodeID) {
		String query = String.format("UPDATE appDB SET cpu='%s', ram='%s', disk='%s', ts='%s', node_id='%s' " +
				"WHERE app_id='%s'", cpu, ram, disk, new Timestamp(ts), nodeID, appID);

		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void deleteAppFromTable(String id) {
		String query = String.format("DELETE FROM appDB WHERE app_id='%s'", id);
		int status;
		Statement statement = null;
		try {
			statement = connection.createStatement();
			status = statement.executeUpdate(query);
			if (status == 1) {
				//Logger.log(id + " deleted an app from appDB", Logger.Level.INFO);
			} else {
				Logger.log(Constants.ERROR+"Error deleting app " + id + " from appDB", Logger.Level.ERROR);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void addLog(String log) {
		String query = "INSERT INTO logDB (log, ts) VALUES (?, ?)";
		PreparedStatement preparedStatement = null;

		try {
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, log);
			preparedStatement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public DBLog[] getLogs(String query) {
		ResultSet resultSet;
		Statement statement = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			List<DBLog> dbLogs = new ArrayList<>();
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String log = resultSet.getString("log");
				Long timestamp = resultSet.getTimestamp("ts").getTime();
				DBLog dbLog = new DBLog(id, log, timestamp);
				dbLogs.add(dbLog);
			}
			return dbLogs.toArray(new DBLog[0]);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}

package services;

import application.Application;
import fi.iki.elonen.NanoHTTPD;
import helpers.DockerUtil;
import helpers.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SensorServer extends NanoHTTPD {

	public SensorServer() {
		super(8081);
		try {
			start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
			Logger.log("\nRunning! Point your browsers to http://localhost:8081 \n", Logger.Level.INFO);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Response serve(IHTTPSession session) {
		Map<String, String> files = new HashMap<>();
		Method method = session.getMethod();
		if (Method.PUT.equals(method) || Method.POST.equals(method)) {
			try {
				session.parseBody(files);
			} catch (IOException ioe) {
				System.out.println("SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
			} catch (ResponseException re) {
				System.out.println(re.getMessage());
			}
		}
		//Post grafana
		Application.influx.newSensorData(files.get("postData"));
		return newFixedLengthResponse(files.get("postData")); // Or postParameter.
	}
}

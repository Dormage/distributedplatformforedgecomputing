package services;

import application.Application;
import common.Node;
import configs.NetworkConfig;
import helpers.Constants;
import helpers.Logger;

public class PeerDiscoveryThread extends Thread {
	public String parameters[];

	public PeerDiscoveryThread(String potencial_node) {
		this.parameters = potencial_node.split(",");
	}

	public void run() {
		String ip = parameters[0];
		int port = Integer.parseInt(parameters[1]);
		String id = parameters[2];
		if (!Application.known_nodes.containsKey(id) && !ip.equals(NetworkConfig.local_ip)) { //unknown node
			Node n = new Node(ip, port, id);
			if (n.connect()) {
				Application.executor.execute(n);
			} else {
				Logger.log(Constants.NETWORK + "connection failed! " + id + " reason: " + n.toString(), Logger.Level.INFO);
			}
		} else {
			Logger.log(Constants.NETWORK + "Node" +Constants.RESET + " " + id + " already known", Logger.Level.NETWORK);
		}
	}
}

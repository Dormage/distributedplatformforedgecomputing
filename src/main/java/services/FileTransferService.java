package services;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import common.FileReceiver;
import configs.NetworkConfig;
import helpers.Constants;
import helpers.Logger;

public class FileTransferService extends Thread {
	public static ServerSocket serverSocket;
	public static ExecutorService executor;

	public FileTransferService() {
		executor = Executors.newCachedThreadPool();
	}

	public void run() {
		while (true) {
			try {
				if (serverSocket == null) {
					serverSocket = new ServerSocket(NetworkConfig.file_transfer_server_port);
					Logger.log(Constants.DOCKER + "File Transfer Service running on: " + NetworkConfig.server_port, Logger.Level.TRANSFER);
				}
			} catch (IOException e) {
				Logger.log(Constants.DOCKER + "Failed to aquire socket: " + NetworkConfig.server_port, Logger.Level.TRANSFER);
				e.printStackTrace();
			}
			try {
				Socket client = serverSocket.accept();
				//fork to transfer file thread
				FileReceiver ft = new FileReceiver(client);
				executor.execute(ft);
				Logger.log(Constants.DOCKER + "Accepted socket connection from " + client.getInetAddress() + ":" + client.getPort(), Logger.Level.TRANSFER);
			} catch (IOException e) {
				Logger.log(Constants.DOCKER + "Client failed to connect", Logger.Level.TRANSFER);
				e.printStackTrace();
			}
		}
	}
}

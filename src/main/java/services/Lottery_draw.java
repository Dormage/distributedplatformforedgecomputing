package services;
import application.Application;
import common.Block;
import configs.NetworkConfig;
import helpers.*;
import protocols.BlockPropagationProtocol;
import java.io.IOException;

public class Lottery_draw extends Thread {
    private Block previous_block;
    Runtime rt;
    Process process;
    Long start;
    Long duration;
    boolean isRunning;
    public Lottery_draw(Block previous_block) {
        this.previous_block = previous_block;
        this.start = System.currentTimeMillis();
        this.isRunning = true;
    }

    @Override
    public void run() {
        if (Application.chain_synced.get()) {
//            System.out.println("Running lottery draw for a ticket: " + previous_block.getTicket());
            rt = Runtime.getRuntime();
            //solve VDF
            if (previous_block != null && isRunning) {
                String proof = null;
                int ticket = Integer.MAX_VALUE;
                //System.out.println(Constants.CONSENSUS + "Running VDF");
                try {
                    long t0 = System.currentTimeMillis();
                    proof = VDF.getVDF(previous_block.getDifficulty(), previous_block.getBlock_hash());
                    Logger.log(Constants.CONSENSUS + "Computed VDF for block: "  + (previous_block.getId()+1), Logger.Level.ERROR);
                    Logger.log(Constants.CONSENSUS + "Difficulty: " + previous_block.getDifficulty() + " hash: " + previous_block.getBlock_hash() + " proof: " + proof, Logger.Level.ERROR);
                    this.duration = (System.currentTimeMillis() - t0);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    process.destroy();
                    Logger.log(Constants.CONSENSUS + " VDF was interrupted by a newer block", Logger.Level.BLOCKCHAIN);
                }

                if (proof != null) {
                    ticket = CryptoUtil.distance(proof, NetworkConfig.node_id);
                }
                Block[] blocks = DatabaseHelper.getBlockByID(previous_block.getId() + 1);

                if (blocks.length > 0) {
                    if (blocks[0].getTicket() > ticket) {
                        Block candidate = BlockchainHelper.forgeBlock(ticket, proof, previous_block);


                        if (candidate != null) {
                            this.isRunning = false;
                            BlockchainHelper.addBlock(candidate);
                            Logger.log(Constants.DOCKER + "New block forged: " + candidate.getId(), Logger.Level.BLOCKCHAIN);
                            BlockPropagationProtocol.broadcastNewBlock(candidate);
                        }
                    }
                } else {
                    Block candidate = BlockchainHelper.forgeBlock(ticket, proof, previous_block);
                    if (candidate != null) {
                        this.isRunning = false;
                        BlockchainHelper.addBlock(candidate);
                        BlockPropagationProtocol.broadcastNewBlock(candidate);
                    }
                }
                Application.influx.newTicket(previous_block, proof, duration, start);
            }
        }
        this.Destroy();
    }
    public boolean Destroy() {
        Logger.log(Constants.CONSENSUS + "Destroying VDF", Logger.Level.BLOCKCHAIN);
        if(process!=null) {
            process.destroyForcibly();
            try {
                process.waitFor();
                this.isRunning = false;
                return true;
            } catch (InterruptedException e) {
                Logger.log(Constants.ERROR + "Cant stop VDF...", Logger.Level.ERROR);
                return false;
            }
        }else {
            return true;
        }
    }

    public boolean isRunning(){
        return this.isRunning;
    }
    public Block getBlock(){
        return this.previous_block;
    }
}

package services;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.spotify.docker.client.messages.ContainerStats;
import common.App;
import common.AppStats;
import configs.NetworkConfig;
import helpers.Constants;
import helpers.DatabaseHelper;
import helpers.DockerUtil;
import helpers.Logger;
import protocols.ResourcePropagationProtocol;

public class ContainerStatsService extends TimerTask {
	Timer timer;
	HashMap<String, AppStats> stats;
	public ContainerStatsService(int interval) {
		timer = new Timer();
		timer.scheduleAtFixedRate(this, new Date(), interval);
	}

	@Override
	public void run() {
		if (DatabaseHelper.getAppByNodeID(NetworkConfig.node_id).apps.length > 0) {
			// compute cpu usage
			//System.out.println(Constants.RESOURCE + "Getting resources");
			try {
				stats = DockerUtil.GetAllContainerStats();
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
			// compute difference
			// System.out.println(Constants.RESOURCE + "Aquire lock");
			for (String key : stats.keySet()) {
				Logger.log(Constants.DOCKER + "Stat key: " + key, Logger.Level.INFO);
				AppStats as = stats.get(key);
				for (App app : DatabaseHelper.getApps().apps) {
					if (app.getId().equals(key)) {
						DatabaseHelper.updateApp(key, key, as.getCPU(), as.getMEM(), as.getIO(), System.currentTimeMillis(), NetworkConfig.node_id);
						break;
					}
				}
			}
			ResourcePropagationProtocol.broadcastResourceStats();
		}
	}

	public double computeCPU(ContainerStats previous, ContainerStats current) {
		double cpuPercent = 0f;
		if (previous != null && current != null) {
			double cpuDelta = current.cpuStats().cpuUsage().totalUsage() - previous.cpuStats().cpuUsage().totalUsage();
			double systemDelta = current.cpuStats().systemCpuUsage() - previous.cpuStats().systemCpuUsage();
			if (systemDelta > 0 && cpuDelta > 0) {
				cpuPercent = (cpuDelta / systemDelta) * current.cpuStats().cpuUsage().percpuUsage().size() * 100;
			}
		}
		return cpuPercent;
	}
}

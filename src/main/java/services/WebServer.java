package services;

import java.io.IOException;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;
import helpers.Constants;
import helpers.DockerUtil;
import helpers.Logger;

public class WebServer extends NanoHTTPD {

	public WebServer() {
		super(8080);
		try {
			start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
		} catch (IOException e) {
			Logger.log(Constants.ERROR +"Error running web server:"+e.toString(),Logger.Level.ERROR);
			e.printStackTrace();
		}
	}

	@Override
	public Response serve(IHTTPSession session) {
		String msg = "<html><body><h1>Run image in a container</h1>\n";
		Map<String, String> parms = session.getParms();
		if (parms.get("image") == null) {
			msg += "<form action='?' method='get'>\n  <p>Your name: <input type='text' name='image'></p>\n"
					+ "</form>\n";
		} else {
			try {
				DockerUtil.startContainer(parms.get("image"));
				msg += "<p>Running image " + parms.get("image") + "!</p>";
			} catch (IOException | InterruptedException e) {
				msg += "<p>Error running image " + e.getMessage() + "!</p>";
			}
		}
		return newFixedLengthResponse(msg + "</body></html>\n");
	}
}

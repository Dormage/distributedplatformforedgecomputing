package configs;

public class NetworkConfig {
	public static int discovery_time = 10000; // initial delay used by peer discovery protocol
	public static int requiered_nodes = 3; // number of required nodes
	public static int server_port = 5000;
	public static int file_transfer_server_port = 5001;
	public static long default_discovery_timeout = 2000L;
	public static String trusted_node_ip = "172.17.0.2";
	//public static String trusted_node_ip = "172.16.150.198";
	public static int trusted_node_port = 5000;
	public static boolean isProduction = false;
	public static String local_ip = "";
	public static int local_port = 0;
	public static String node_id;
	public static Double block_time = 30000d; // in miliseconds
	public static int resource_propagation_time = 10000;
}

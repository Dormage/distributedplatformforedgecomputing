package configs;

public class NodeConfig {
	public static boolean debug_mode = true;
	public static String mount_point = "log/";
	public static boolean write_stats = true;
	public static boolean running_docker= false;
	public static boolean running_host= true;
	public static long max_message_age = 60000L;
}

package helpers;

import application.Application;
import common.Node;
import configs.NodeConfig;
import protobuf.ProtoMessage;
import java.util.Iterator;

public class GossipHelper {
    public static void broadcastMessage(ProtoMessage.Message message, Node originNode) {
        String msgHash = CryptoUtil.Hash(message.toString());
        if (!Application.messageQueue.containsKey(msgHash)) {
            Application.messageQueue.put(msgHash, System.currentTimeMillis());
            System.out.println();
            //clear message queue
            Iterator iter = Application.messageQueue.keySet().iterator();
            while(iter.hasNext()){
                String key = (String) iter.next();
                Long time = Application.messageQueue.get(key);
                if (System.currentTimeMillis() > time + NodeConfig.max_message_age) {
                   iter.remove();
                }
            }
            Application.known_nodes.values().stream().filter(node -> !node.equals(originNode)).forEach((node) -> {
                node.send(message);
//                System.out.println("Sending msg " + msgHash.substring(0, 10) + " to " + node);
            });
        }
    }
}

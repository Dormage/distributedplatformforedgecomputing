package helpers;

import java.util.HashMap;
import application.Application;
import common.Block;
import common.Stat;
import configs.NetworkConfig;
import protocols.BlockchainSyncProtocol;

public class BlockchainHelper {

    public static boolean verifyIntegrity(Block block) {
        return true;
    }

    public static boolean verifyProof(Block candidate) {
        Block previous = DatabaseHelper.getBlockByID(candidate.getId() - 1)[0];
        if (VDF.verifyVDF(previous.getDifficulty(), previous.getBlock_hash(), candidate.getVdf_proof(), candidate)) {
            Logger.log(Constants.CHAIN + "Proof for block " + previous.getId() + " " + Constants.GREEN_BOLD_BRIGHT + "is valid!", Logger.Level.BLOCKCHAIN);
            return true;
        } else {
            Logger.log(Constants.CHAIN +"Proof for block " + previous.getId() +" "+ Constants.RED_BOLD_BRIGHT + "is invalid!", Logger.Level.ERROR);
            return false;
        }
    }
    public static boolean verifyTicket(Block block){
        int ticket = CryptoUtil.distance(block.getVdf_proof(),block.getBlock_producer());
        if(block.getTicket() == ticket){
            return true;
        }else{
            Logger.log(Constants.CONSENSUS + "Ticket verification failed for " + block.getId()+" BlockProducer: "+block.getBlock_hash()+ " Ticket: " +block.getTicket() + "Compared to: " +ticket, Logger.Level.BLOCKCHAIN);
            return false;
        }
    }

    public static void addNewBlocks(Block[] blocks) {
        int count=0;
        for (Block block : blocks) {
            //Logger.log(Constants.BLOCK + "Trying to add block with id: " + block.getId(), Logger.Level.BLOCKCHAIN);
            if (block.getPrevious_block_hash().equals(DatabaseHelper.getLastBlock().getBlock_hash())) {
                DatabaseHelper.addBlock(block);
                count ++;
            }
        }
        Logger.log(Constants.SYNC + "Chain Sync received " +Constants.GREEN + blocks.length + " Added: " +Constants.GREEN + count + " blocks", Logger.Level.BLOCKCHAIN);
        Application.chain_synced.set(true);
    }

    public static void printBlockChain() {
        for (Block block : DatabaseHelper.getAllBlocks()) {
            printBlock(block);
        }
    }

    public static void printBlock(Block block) {
        Logger.log(Constants.CHAIN + "ID:"+ Constants.RESET+ " " + block.getId(), Logger.Level.BLOCKCHAIN);
        Logger.log(Constants.CHAIN + "Previous Block Hash:"+ Constants.RESET+ " "  + block.getPrevious_block_hash(), Logger.Level.BLOCKCHAIN);
        Logger.log(Constants.CHAIN + "Current Block Hash:"+ Constants.RESET+ " "  + block.getBlock_hash(), Logger.Level.BLOCKCHAIN);
        Logger.log(Constants.CHAIN + "Difficulty"+ Constants.RESET+ " "  + block.getDifficulty(), Logger.Level.BLOCKCHAIN);
        Logger.log(Constants.CHAIN + "Winning ticket:"+ Constants.RESET+ " "  + block.getTicket(), Logger.Level.BLOCKCHAIN);
        Logger.log(Constants.CHAIN + "Forged by:"+ Constants.RESET+ " "  + block.getBlock_producer(), Logger.Level.BLOCKCHAIN);
        //Logger.log(Constants.CHAIN + "Proof " + block.getVdf_proof(), Logger.Level.BLOCKCHAIN);
        if (block.data != null) {
            for (Stat app : block.data.values()) {
                Logger.log(Constants.CHAIN + "App"+ Constants.RESET + " " + app.getId(), Logger.Level.BLOCKCHAIN);
                Logger.log(Constants.CHAIN + "Node"+ Constants.RESET+ " "  + app.getNode_id(), Logger.Level.BLOCKCHAIN);
                Logger.log(Constants.CHAIN + "Resources"+ Constants.RESET+ " "  + app.getCpu(), Logger.Level.BLOCKCHAIN);
            }
        }
        Logger.log(Constants.CHAIN + "#####################", Logger.Level.BLOCKCHAIN);

    }

    /*Forges a candidate block that includes the ticket drawn and a VDF proof for other nodes to verify
     * */
    public static Block forgeBlock(int ticket, String proof, Block prevBlock) {
        Block[] current_blocks = DatabaseHelper.getBlockByID(prevBlock.getId());
        Block current_block = null;

        if (current_blocks != null && current_blocks.length > 0) {
            current_block = current_blocks[0];
        }

        Block candidate;
        if (current_block != null) {
            HashMap<String, Stat> plan = MigrationHelper.createMigrationPlan(current_block);

            candidate = new Block(
                    current_block.getBlock_hash(),
                    current_block.getId() + 1,
                    ticket,
                    adjustDifficulty(prevBlock),
                    proof,
                    NetworkConfig.node_id,
                    plan,
                    false);

            Application.influx.newBlockForged(candidate);

            return candidate;
        } else {
            return null;
        }
    }

    public static void makeBlockFinal(Block block) {
        block.setBlock_final(true);
        DatabaseHelper.addBlock(block);
        DatabaseHelper.addLog("Finalizing block " + block.getId());
        Logger.log(Constants.SYNC + "Finalizing block "+Constants.GREEN_BOLD_BRIGHT + block.getId(),Logger.Level.BLOCKCHAIN);
        MigrationHelper.executeMigrationPlan(block);
    }

    public synchronized static void addBlock(Block candidate) {
        if (Application.chain_synced.get() || Application.incommingBlock.get() > 2) {
            Application.incommingBlock.set(0);
            Block current = DatabaseHelper.getLastBlock();

            if (current.getId() == candidate.getId() - 1 ) { //future
                if (candidate.getPrevious_block_hash().equals(current.getBlock_hash())) {
                    DatabaseHelper.addBlock(candidate);
                    DatabaseHelper.addLog("Adding block: " + candidate.getId() + " N+1");
                    Logger.log(Constants.CHAIN + "A new block was included at height" +Constants.RESET+ " "+ candidate.getId(), Logger.Level.BLOCKCHAIN);
                    Application.chain_synced.set(true);
                    if(verifyProof(candidate) && verifyTicket(candidate)){
                        //TODO:this is where we can take a shortcut and drop our VDF compute since the proof will be the same.
                       /* if(LotteryHelper.isRunning()){
                            if(LotteryHelper.stop()){
                                Logger.log(Constants.CHAIN + "Stopped current VDF, and start working on the next block", Logger.Level.BLOCKCHAIN);
                                int ticket = CryptoUtil.distance(candidate.getVdf_proof(), NetworkConfig.node_id);
                                if(candidate.getTicket()>ticket){
                                    Block myBlock = BlockchainHelper.forgeBlock(ticket, candidate.getVdf_proof(), current);
                                    BlockPropagationProtocol.broadcastNewBlock(myBlock);
                                    Logger.log(Constants.CHAIN + "My ticket is better, preparing block and broadcasting", Logger.Level.BLOCKCHAIN);
                                }
                                LotteryHelper.drawLottery(candidate);
                            }else{
                                Logger.log(Constants.CHAIN + "Was unable to stop the current VDF", Logger.Level.BLOCKCHAIN);
                            }
                        }*/
                    }else{
                        return;
                    }
                    if (!LotteryHelper.isRunning()) {
                        if (NetworkConfig.node_id.equals(candidate.getBlock_producer())) {
                            Logger.log(Constants.CHAIN + "Block was mine, start working on a new one", Logger.Level.BLOCKCHAIN);
                            LotteryHelper.drawLottery(candidate);
                        } else {
                            LotteryHelper.drawLottery(candidate);
                            Logger.log(Constants.CHAIN + "Start working on forging a new block for height:" +Constants.RESET+" "+ (candidate.getId() + 1), Logger.Level.BLOCKCHAIN);
                        }
                    }else{
                        Logger.log(Constants.CHAIN + "The VDF appears to be running:" + Constants.RED_BOLD_BRIGHT + LotteryHelper.isRunning(), Logger.Level.BLOCKCHAIN);
                    }
                } else {
                    Logger.log(Constants.SYNC + "Possible fork detected. Backtracking...", Logger.Level.FORK);
                    DatabaseHelper.deleteBlock(candidate.getId() - 1);
                    BlockchainSyncProtocol.requestBlocks(NetworkUtil.getRandomNode());
                    DatabaseHelper.addLog("Deleting block: "  + (candidate.getId() - 1) +"N-1");
                    Application.influx.newPossibleFork(candidate.getId(), candidate.getBlock_producer(), candidate.getTicket());
                }
            } else if (candidate.getId() == current.getId()) { //present
                if(verifyProof(candidate) && verifyTicket(candidate)){
                    //TODO:this is where we can take a shortcut and drop our VDF compute since the proof will be the same.
                }else{
                    return;
                }
                if (candidate.getPrevious_block_hash() != null && current.getBlock_hash() != null && candidate.getPrevious_block_hash().equals(current.getPrevious_block_hash())) { //they both reference the same previous block
                    if (candidate.getTicket() < current.getTicket()) { //accept the lottery winner
                        DatabaseHelper.addBlock(candidate);
                        LotteryHelper.drawLottery(candidate);
                        DatabaseHelper.addLog("Updating block: " + candidate.getId());
                    }
                }
            } else if (candidate.getId() < current.getId()) { //past
                Block[] conflictingBlocks = DatabaseHelper.getBlockByID(candidate.getId());
                Block conflicting = new Block();

                if (conflictingBlocks != null && conflictingBlocks.length > 0) {
                    conflicting = conflictingBlocks[0];
                    if (conflicting.getTicket() > candidate.getTicket() && conflicting.getId() != 0) { //fork happened we have to sync
                        Logger.log(Constants.CHAIN + "Fork detected, resolving...", Logger.Level.FORK);
                        Application.influx.fork_detected(conflicting);
                        for (Block b : DatabaseHelper.getAllBlocks()) {
                            if (b.getId() > conflicting.getId()) {
                                DatabaseHelper.deleteBlock(b.getId());
                                DatabaseHelper.addLog("Deleting block: " + b.getId() + " FORK");
                            }
                        }
                        Logger.log(Constants.CHAIN + "Forced a chain resync from block " + candidate.getId(), Logger.Level.FORK);
                        BlockchainSyncProtocol.requestBlocks(NetworkUtil.getRandomNode());
                    } else {
                        Logger.log(Constants.CHAIN + "The conflicting block"+Constants.YELLOW + " " + candidate.getId() +" is not the lottery winner, rejecting...", Logger.Level.FORK);
                    }
                }
            } else { //our VDF (if still running) is invalidated, we must accept the blocks and work on forging the new one
                Logger.log(Constants.CHAIN + "Our VDF is invalidated by block " + Constants.YELLOW + candidate.getId() + Constants.RESET+" | start working on block "+ Constants.GREEN + (candidate.getId() + 1) + " and force a chain resync", Logger.Level.FORK);
                LotteryHelper.drawLottery(candidate);
                if (current.getId() != 0) {
                    DatabaseHelper.deleteBlock(current.getId());
                    DatabaseHelper.addLog("Deleting block: " + current.getId() + " ELSE");

                }
                BlockchainSyncProtocol.requestBlocks(NetworkUtil.getRandomNode());
            }

            if (candidate.getId() > 2) {
                Block[] lastBlocks = DatabaseHelper.getBlocksFromID(candidate.getId() - 2);
                if (lastBlocks != null && lastBlocks.length > 0 && !lastBlocks[0].getBlock_final()) {
                    makeBlockFinal(lastBlocks[0]);
                    Application.influx.newBLockAccepted(lastBlocks[0]);
                }
            }
        } else {
            if (candidate.getId() > Application.lastCandidate.getId()) {
                Application.incommingBlock.getAndIncrement();
                Application.lastCandidate = candidate;
            }
        }
    }

    public static int adjustDifficulty(Block previous_block) {
        //adjust difficulty
        long delta_t = System.currentTimeMillis() - previous_block.getTimestamp();
        //basic logic is, increase difficulty if block was forged faster then desired block time, and increase if it took longer.
        double ratio = (delta_t / NetworkConfig.block_time);
        Logger.log(Constants.CHAIN + "Adjusting difficulty..." + delta_t + " : " + NetworkConfig.block_time + ":" + ratio, Logger.Level.INFO);
        double new_difficulty = 0;
        if (ratio > 0) { //increase difficulty
            new_difficulty = previous_block.getDifficulty() + (previous_block.getDifficulty() * (1 - ratio));
            Logger.log(Constants.CHAIN + "Adjusting difficulty..." + new_difficulty, Logger.Level.INFO);
            if (new_difficulty <= 0) {
                return 100;
            }
            return (int) new_difficulty;
        } else if (ratio < 0) { //decrease difficulty
            new_difficulty = previous_block.getDifficulty() - (previous_block.getDifficulty() * (1 - ratio));
            Logger.log(Constants.CHAIN + "Adjusting difficulty..." + new_difficulty, Logger.Level.INFO);
            if (new_difficulty <= 0) {
                return 100;
            }
            return (int) new_difficulty;
        } else {
            return previous_block.getDifficulty();
        }
    }
}

package helpers;

import common.AppsWithStats;
import common.Block;
import common.DBLog;
import common.NodeModel;
import services.Database;

import java.sql.Timestamp;

public class DatabaseHelper {
	
	public static Database db;

	public static void initDB(String table) {
		db = new Database(table);

	}

	// Known nodes *****************************************************************************************************
	public static NodeModel[] getAllKnownNodes() {
		String query = "SELECT * FROM knownNodes";
		return db.getKnownNodes(query, "All");
	}

	public static void addNewNode(String nodeID, String ip, int port, Timestamp ping) {
		db.addNodeToKnownNodes(nodeID, ip, port, ping);
	}

	public static void removeNodeFromKnownNodes(int id) {
		db.deleteFromTable("knownNodes", id);
	}

	// Block ***********************************************************************************************************
	public static void addBlock(Block block) {
		db.addBlockToDB(block);
	}

	public static Block getLastBlock() {
		return db.getBlocks("SELECT * FROM blockDB ORDER BY block_id DESC LIMIT 1")[0];
	}

	public static Block[] getBlocksFromID(int id) {
		String query = "SELECT * FROM blockDB WHERE block_id >= '" + id + "' LIMIT 5000";
		return db.getBlocks(query);
	}

	public static Block[] getBlocksFromTo(int start, int end) {
		String query = "SELECT * FROM blockDB WHERE block_id >= '" + start + "' LIMIT " + end;
		System.out.println("Query: " + query);
		return db.getBlocks(query);
	}

	public static Block[] getAllBlocks() {
		String query = "SELECT * FROM blockDB";
		return db.getBlocks(query);
	}

	public static Block[] getBlockByID(int id) {
		String query = "SELECT * FROM blockDB WHERE block_id = '" + id + "'";
		return db.getBlocks(query);
	}

	public static void deleteBlock(int id) {
		db.deleteBlockFromTable(id);
	}

	// App *************************************************************************************************************
	public static void addApp(String appID, String dID, String name, double cpu, double ram, double disk, long ts, String nodeID) {
		db.addAppToDB(appID, dID, name, cpu, ram, disk, ts, nodeID);
	}

	public static void updateApp(String appID, String dID, double cpu, double ram, double disk, long ts, String nodeID) {
		db.updateApp(appID, dID, cpu, ram, disk, ts, nodeID);
	}

	public  static void deleteApp(String id) {
		db.deleteAppFromTable(id);
	}

	public static AppsWithStats getApps() {
		String query = "SELECT * FROM appDB";
		return db.getApps(query);
	}

	public static AppsWithStats getAppByNodeID(String id) {
		String query = String.format("SELECT * FROM appDB WHERE node_id = '%s'", id);
		return db.getApps(query);
	}

	public static void addLog(String log) {
		db.addLog(log);
	}

	public static DBLog[] getLogs(int offset) {
		String query = String.format("SELECT * FROM logDB WHERE id >= '%s'", offset);
		return db.getLogs(query);
	}

	public static void closeConnections() {
		db.close();
	}
}

package helpers;

import application.Application;

public class Logger {
    public enum Level {
        ERROR,
        WARNING,
        INFO,
        DEBUG,
        FORK,
        BLOCKCHAIN,
        NETWORK,
        TRANSFER
    }

    public static void log(String message, Level level) {
        for (Level l : Application.logConfig) {
            if (l == level) {
                System.out.println(message);
            }
        }
    }
}

package helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.*;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import application.Application;
import common.*;
import configs.NetworkConfig;

public class DockerUtil {
	public static DockerClient docker = null;
	public static boolean isConnected = false;

	public static void init() {
		docker = new DefaultDockerClient("unix:///var/run/docker.sock");
		if (docker != null) {
			isConnected = true;
			Logger.log(Constants.DOCKER + "Connected to docker daemon!", Logger.Level.INFO);
		}
	}

	public static HashMap<String, AppStats> GetAllContainerStats() throws IOException, InterruptedException {
		//make a list of containers we are querying
		HashMap<String, AppStats> result= new HashMap<String, AppStats>();
		String container_list="";
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec("docker stats --no-stream");
		StringBuilder output = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line).append("\n");
		}
		int exitVal = process.waitFor();
		if (exitVal == 0) {
			String[] lines = output.toString().split(System.getProperty("line.separator"));
			//we have the table now cross reference with our apps
			AppsWithStats appsWithStats = DatabaseHelper.getApps();

			for (int i = 0; i < appsWithStats.apps.length; i++) {
				App app = appsWithStats.apps[i];
				if (appsWithStats.stats[i].getNode_id().equals(NetworkConfig.node_id)) {
					//find it in the output
					for (int j = 0; j < lines.length; j++) {
						String outline = lines[j].replaceAll("\\s+", " ");
						String details[] = outline.split(" ");
						if(details[0].contains(app.getDid())){
							//System.out.println(outline);
							for (int k =0; k < details.length;k++){
								double CPU = Double.parseDouble(details[2].replace("%",""));
								double MEMA = Double.parseDouble(details[3].replaceAll("[^\\d.]", ""));
								double MEMT = Double.parseDouble(details[5].replaceAll("[^\\d.]", ""));
								double MEM = MEMT /MEMA;
								double NET =0; //TODO
								double IO=0;
								//System.out.println(Constants.DOCKER+ "App: "+ app.getName()	);
								AppStats as = new AppStats(CPU,MEM,IO,NET);
								//System.out.println(Constants.DOCKER+ as.toString());
								result.put(app.getName(), as);
							}
						}
					}
				}
			}
		} else {
			Logger.log(Constants.ERROR + "Error getting container stats", Logger.Level.ERROR);
			return result;
		}
		return result;
	}

	public static void printBlockDB(int start, int end) {
//		System.out.println(Constants.BLOCK + "List of blocks in DB:");
		Block[] blocks = DatabaseHelper.getBlocksFromTo(start, end);
		for (Block block : blocks) {
			System.out.println(
				Constants.SYNC + "Block ID: " + block.getId() +
					" producer: " + block.getBlock_producer() +
					" block hash: " + block.getBlock_hash() +
					" previous hash: " + block.getPrevious_block_hash() +
					" ticket: " + block.getTicket() +
					" difficulty: " + block.getDifficulty() +
					" timestamp: " + block.getTimestamp() +
					" final: " + block.getBlock_final()
			);

//			if (block.data != null && block.data.size() > 0) {
//				for (Stat s: block.data.values()
//				) {
//					System.out.println("App ID: " + s.getId());
//					System.out.println("with CPU: " + s.getCpu());
//					System.out.println("go to NodeID: " + s.getNode_id());
//				}
//			}
		}
	}

	public static void printKnownNodesDB() {
		System.out.println(Constants.SYNC + "List of known nodes in DB:");
		NodeModel[] knownNodes = DatabaseHelper.getAllKnownNodes();
		for (NodeModel nodeModel : knownNodes) {
			System.out.println(Constants.SYNC + "Node ID: " + nodeModel.getId() + " ip: " + nodeModel.getIp() + " port: " + nodeModel.getPort() + " ping: " + nodeModel.getPing());
		}
	}

	public static void printAppsWithStats() {
		AppsWithStats appsWithStats = DatabaseHelper.getApps();
        System.out.println(Constants.APP + "List of apps in db:");
        for (int i = 0; i < appsWithStats.apps.length; i++) {
            System.out.println(Constants.APP + "appID: " + appsWithStats.apps[i].getId() + " containerID: " + appsWithStats.apps[i].getDid() +
					" appName: " + appsWithStats.apps[i].getName() + " CPU: " + appsWithStats.stats[i].getCpu() + " nodeID: " + appsWithStats.stats[i].getNode_id());
		}
	}

    public static void printRunningAppsFromDB() {
        AppsWithStats appsWithStats = DatabaseHelper.getApps();
        System.out.println(Constants.APP + "List of running apps:");
        for (int i = 0; i < appsWithStats.stats.length; i++) {
            if (appsWithStats.stats[i].getNode_id().equals(NetworkConfig.node_id)) {
                System.out.println(Constants.APP + "appID: " + appsWithStats.apps[i].getId() + " containerID: " + appsWithStats.apps[i].getDid() +
						" appName: " + appsWithStats.apps[i].getName() + " CPU: " + appsWithStats.stats[i].getCpu() + " nodeID: " + appsWithStats.stats[i].getNode_id());
            }
        }
    }

	public static void printNodeID() {
		System.out.println(Constants.DOCKER + "Node ID: " + NetworkConfig.node_id);
	}

	public static void printLogs(int offset) {
		DBLog[] logs = DatabaseHelper.getLogs(offset);
		for (DBLog log : logs) {
			System.out.println(Constants.SYNC + "Log " + log.getId() + " " + log.getLog() + "; " + new Timestamp(log.getTimestamp()));
		}
	}

	public static String executeCommand(String command) throws IOException, InterruptedException {
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec(command);
		StringBuilder output = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line + "\n");
		}
		int exitVal = process.waitFor();
		if (exitVal == 0) {
			return output.toString();
		} else {
			return "error";
		}
	}

	public static String startContainer(String imageId) throws IOException, InterruptedException {
		String container_id;
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec("docker run -d " + imageId);
		StringBuilder output = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line + "\n");
		}
		int exitVal = process.waitFor();
		if (exitVal == 0) {
			container_id = output.toString().substring(0, 5);
			container_id = container_id.replaceAll("\\s+", "");
			Logger.log(Constants.DOCKER + "Created container " + container_id, Logger.Level.TRANSFER);

			DatabaseHelper.addApp(container_id, container_id, container_id, 0, 0, 0, System.currentTimeMillis(), NetworkConfig.node_id);

			return container_id;
		} else {
			Logger.log(Constants.ERROR + "Error creating container " + imageId, Logger.Level.ERROR);
			return null;
		}
	}

	public static String startTransferedContainer(String imageId) throws IOException, InterruptedException {
		String container_id;
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec("docker run -d " + imageId);
		StringBuilder output = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line + "\n");
		}
		int exitVal = process.waitFor();
		if (exitVal == 0) {
			container_id = output.toString().substring(0, 5);
			container_id = container_id.replaceAll("\\s+", "");
			Logger.log(Constants.DOCKER + "Created transported container " + container_id, Logger.Level.TRANSFER);
			return container_id;
		} else {
			Logger.log(Constants.ERROR + "Error creating transported container " + imageId, Logger.Level.ERROR);
			return null;
		}
	}

	public static String exportContainer(String containerId, String appID) throws IOException, InterruptedException {
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec("docker commit " + containerId + " " + containerId);
		StringBuilder output = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line + "\n");
		}
		int exitVal = process.waitFor();
		if (exitVal == 0) {
//			Vector<App> myApps = new Vector<>(Arrays.asList(DatabaseHelper.getApps().apps));
			Logger.log(Constants.DOCKER + " exported container to image: " + output.toString().replaceAll("sha256:", "").substring(0, 5), Logger.Level.TRANSFER);
//			myApps.stream().filter(app -> app.getId().equals(appID)).findFirst().get()
//					.setDid(output.toString().replaceAll("sha256:", "").substring(0, 5));
			return output.toString().replaceAll("\\s+", "").replaceAll("sha256:", "").substring(0, 5);
		} else {
			return null;
		}
	}
	public static void stopContainer (String containerId) throws IOException, InterruptedException {

		Runtime rt = Runtime.getRuntime();
		ProcessBuilder builder = new ProcessBuilder("docker", "stop", containerId);
		Process process = builder.start();

		StringBuilder output = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line + "\n");
		}
		int exitVal = process.waitFor();
		if (exitVal == 0) {
			Logger.log(Constants.DOCKER + "Sucessfully stopped image", Logger.Level.TRANSFER);
		} else {
			Logger.log(Constants.ERROR + "Error stopping image " + containerId + " exit code " + exitVal, Logger.Level.ERROR);
		}
	}

	public static void compressImage(String imageId) throws IOException, InterruptedException {
		Runtime rt = Runtime.getRuntime();
		ProcessBuilder builder = new ProcessBuilder("docker", "save", imageId);
		builder.redirectOutput(ProcessBuilder.Redirect.to(new File(imageId + ".tar")));
		Process process = builder.start();

		StringBuilder output = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line + "\n");
		}
		int exitVal = process.waitFor();
		if (exitVal == 0) {
			Logger.log(Constants.DOCKER + "Sucessfully compressed image", Logger.Level.TRANSFER);
		} else {
			Logger.log(Constants.ERROR + "Error compressing image " + imageId + " exit code " + exitVal, Logger.Level.ERROR);
		}
	}

	public static String loadImage(String URI, String containerId) throws IOException, InterruptedException {
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec("docker load -i " + URI);
		StringBuilder output = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line).append("\n");
		}
		int exitVal = process.waitFor();
		if (exitVal == 0) {
			Logger.log(Constants.DOCKER + "Successfully loaded image ", Logger.Level.TRANSFER);
			Logger.log("Command output " + output.toString(), Logger.Level.TRANSFER);
			String container_id = output.toString().replaceAll("sha256:", "").replace("Loaded image ID: ", "").replaceAll("\\s+", "").substring(0, 5);
			Logger.log(Constants.DOCKER + " Added container " + containerId + " to managed apps.", Logger.Level.TRANSFER);
			return container_id;
		} else {
			Logger.log(Constants.ERROR + "Error compressing image " + URI + " exit code " + exitVal, Logger.Level.ERROR);
			return null;
		}
	}

	public static void removeContainer(String containerId) throws IOException, InterruptedException {
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec("docker rm -f " + containerId);
		StringBuilder output = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line).append("\n");
		}
		int exitVal = process.waitFor();
		if (exitVal == 0) {
			Logger.log(Constants.DOCKER + "Successfully removed container " , Logger.Level.TRANSFER);
		} else {
			Logger.log(Constants.ERROR + "Error removing container " + containerId + " exit code " + exitVal, Logger.Level.ERROR);

		}
	}

	public static void migrateContainer(String containerId, Node node, String app_name, int blockID, String blockProducer) {
		// export container to image
		String image_id = null;
		try {
			image_id = exportContainer(containerId, app_name);
			Logger.log(Constants.MIGRATION + "container " + containerId + " exported to image " + image_id, Logger.Level.TRANSFER);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

		try {
			stopContainer(containerId);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

		// compress image
		try {
			compressImage(image_id);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

		// transfer image
		Application.executor.execute(new FileSender(image_id + ".tar", node.getAddress(), NetworkConfig.file_transfer_server_port, app_name, node.getId(), blockID, blockProducer));
		Logger.log(Constants.DOCKER + "Removing container " + containerId + " from managed apps", Logger.Level.TRANSFER);
	}


	public static void removeCompressedContainer(String container) throws IOException, InterruptedException {
		System.out.println("URI: " + container);
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec("rm -f " + container);
		StringBuilder output = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line).append("\n");
		}
		int exitVal = process.waitFor();
		if (exitVal == 0) {
			Logger.log(Constants.DOCKER + "Successfully removed compressed container " + container, Logger.Level.TRANSFER);
		} else {
			Logger.log(Constants.ERROR + "Error removing compressed container " + container + "; exit code " + exitVal, Logger.Level.ERROR);

		}
	}
}

package helpers;

import common.Block;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class VDF {
	
	public static String getVDF(int difficulty, String hash) throws IOException, InterruptedException {
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec("vdf-cli "+ hash+ " "+difficulty);
		StringBuilder output = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line + "\n");
		}
		int exitVal = process.waitFor();
		if (exitVal == 0) {
			return output.toString();
		} else {
			Logger.log(Constants.ERROR + "Error producing VDF" + exitVal, Logger.Level.ERROR);
			return null;
		}
	}

	public static boolean verifyVDF(int difficulty, String hash, String proof, Block candidate){
		Runtime rt = Runtime.getRuntime();
		try {
			Process process = rt.exec("vdf-cli " + hash + " " + difficulty + " "+ proof);
			StringBuffer out = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while((line = reader.readLine())!=null){
				out.append(line+"\n");
			}
			int exit = 0;
			try {
				exit = process.waitFor();
			} catch (InterruptedException e) {
				return false;
			}
			if(exit==0){ //seccess, parse out
				if(out.toString().trim().equals("Proof is valid")){
					return true;
				}else{
					return false;
				}
			}else{
				Logger.log(Constants.ERROR + "Error verifying VDF " + out.toString().trim() + " for block: " + candidate.getId(), Logger.Level.ERROR);
				Logger.log(Constants.ERROR + "Difficulty: " + difficulty + " hash: " +hash + " proof: " + proof, Logger.Level.ERROR);
				return false;
			}
		} catch (IOException e) {
			//should never happen, reject block if it does and resync
			return false;
		}

	}
}

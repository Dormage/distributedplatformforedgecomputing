package helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import application.Application;
import common.App;
import common.AppsWithStats;
import common.Block;
import common.Stat;
import configs.NetworkConfig;
import jnr.constants.Constant;

public class MigrationHelper {

	public static int executeMigrationPlan(Block block) {
		// executes migration plan by sending docker images to respective nodes
		// if images need to be migrated to this node, it waits for them to be received and
		// loads them.
		printPlan(block);
		int num_migrations = 0;

		AppsWithStats appsWithStats = DatabaseHelper.getApps();

		for (int i = 0; i < appsWithStats.apps.length; i++) {
			App local = appsWithStats.apps[i];
			Stat localStat = appsWithStats.stats[i];
			for (Stat stat : block.data.values()) { // go through migration plan
				if (localStat.getNode_id().equals(NetworkConfig.node_id) && local.getName().equals(stat.getId()) && !stat.getNode_id().equals(NetworkConfig.node_id)) {
					// migrate app
					System.out.println(Constants.MIGRATION + "Migrating container " + stat.getId() + " from " + NetworkConfig.node_id + " to " + stat.getNode_id());
					DockerUtil.migrateContainer(stat.getId(), Application.known_nodes.get(stat.getNode_id()), local.getName(), block.getId(), block.getBlock_producer());
					Application.influx.newMigration(local, stat);
					num_migrations++;
				}
			}
		}

		return num_migrations;
	}

	public static HashMap<String, Stat> createMigrationPlan(Block block) {
		System.out.println(Constants.MIGRATION + "Preparing migration plan");
		HashMap<String, Stat> current_state;

		AppsWithStats appsWithStats = DatabaseHelper.getApps();

		if (block.data != null && block.data.size() == 0) {
			current_state = new HashMap<>();

			for (int i = 0; i < appsWithStats.apps.length; i++) {
				current_state.put(appsWithStats.apps[i].getId(), appsWithStats.stats[i]);
			}

			System.out.println(Constants.MIGRATION + "No block data, no plan!");
			// current_state.putAll(Application.local_resource_stats);
			return current_state;
		} else {
			// join resource stats with local
			current_state = block.data;

			HashMap<String, ArrayList<Stat>> per_node = new HashMap<>();

			if (current_state != null) {
				for (String key : current_state.keySet()) {
					if (!per_node.containsKey(current_state.get(key).getNode_id())) {
						ArrayList<Stat> tmp = new ArrayList<Stat>();
						tmp.add(current_state.get(key));
						per_node.put(current_state.get(key).getNode_id(), tmp);
					} else {
						per_node.get(current_state.get(key).getNode_id()).add(current_state.get(key));
					}
				}
			}

			for (String node_id : Application.known_nodes.keySet()) {
				if (!per_node.containsKey(node_id)) {
					per_node.put(node_id, new ArrayList<>());
				}
			}

			if (!per_node.containsKey(NetworkConfig.node_id)) {
				per_node.put(NetworkConfig.node_id, new ArrayList<>());
			}

			// find max node
			String max_node = null;
			String min_node = null;
			Stat min_app = new Stat("", Double.MAX_VALUE,0,0, 0, "");

			int max_node_resources = 0;
			int min_node_resources = Integer.MAX_VALUE;
			for (String node_id : per_node.keySet()) {
				int sum_node = 0;
				for (Stat app : per_node.get(node_id)) {
					sum_node += app.getCpu();
				}
				if (max_node_resources <= sum_node) {
					max_node = node_id;
					max_node_resources = sum_node;
				}
				if (min_node_resources >= sum_node) {
					min_node = node_id;
					min_node_resources = sum_node;
				}
			}
			// find min app
			for (Stat app : per_node.get(max_node)) {
				if (min_app.getCpu() > app.getCpu()) {
					min_app = app;
				}
			}

			System.out.println(Constants.MIGRATION + "Max node " + max_node);
			System.out.println(Constants.MIGRATION + "Min node " + min_node);
			// candidate replacement found, we check if it would benefit the balance
			double origin_resources = max_node_resources - min_app.getCpu();
			double destination_resources = min_node_resources + min_app.getCpu();

			if ((Math.abs(max_node_resources - min_node_resources) > Math.abs(origin_resources - destination_resources)) && max_node_resources > 10) {
				System.out.println(Constants.MIGRATION + "Need to migrate app " + min_app.getId() + " from node " + min_app.getNode_id() + " to node " + min_node);
				if (current_state != null) {
					current_state.get(min_app.getId()).setNode_id(min_node);
				}
				//System.out.println(Constants.ERROR + current_state.get(min_app.getId()).getNode_id());
			}

			// current_state.putAll(Application.resource_pool);
			for (int i = 0; i < appsWithStats.apps.length; i++) {
				if (current_state != null) {
					Stat dbStat = appsWithStats.stats[i];
					String nodeID = dbStat.getNode_id();

					for (Map.Entry<String, Stat> entry : current_state.entrySet()) {
						if (entry.getValue().getId().equals(dbStat.getId())) {
							nodeID = entry.getValue().getNode_id();
						}
					}

					Stat stat = new Stat(
							dbStat.getId(),
							dbStat.getCpu(),
							dbStat.getRam(),
							dbStat.getDisk(),
							dbStat.getTimestamp(),
							nodeID
					);
					current_state.put(appsWithStats.apps[i].getId(), stat);
				}
			}
			return current_state;
		}
	}

	public static void printPlan(Block block) {
		if (block.data != null) {
			for (Stat s : block.data.values()) {
				System.out.println(Constants.MIGRATION + s.getId() + " | " + s.getNode_id() + " | " + s.getCpu());
			}
		}
	}
}

package helpers;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class CryptoUtil {

	public static String Hash(String message) {
		MessageDigest md;
		String hex = "";
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(message.getBytes(StandardCharsets.UTF_8));
			byte[] digest = md.digest();
			hex = String.format("%064x", new BigInteger(1, digest));
			return hex;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hex;
	}

	public static boolean Verify(String message, String hash) {
		String received = null;
		received = Hash(message);
		if (received.equals(hash)) {
			return true;
		} else {
			return false;
		}
	}

	public static int distance(String proof, String node_id){
		Long seed = Long.parseUnsignedLong(proof.substring(0,16),16);
		Random random = new Random(seed);
		double draw = random.nextDouble();
		double front = Math.abs(Long.parseUnsignedLong(node_id.substring(0,16),16) / (double)Long.MAX_VALUE);
		int ticket = (int) (Math.abs(front - draw) * 1000000);
		Logger.log(Constants.CONSENSUS +"Draw: " +draw + " Node: " + front + " distance: " + ticket , Logger.Level.BLOCKCHAIN);
		return ticket;
	}


	public static int levenshteinDistance (String lhss, String rhss) {
		CharSequence lhs = lhss;
		CharSequence rhs = rhss;
		int len0 = lhs.length() + 1;
		int len1 = rhs.length() + 1;

		// the array of distances
		int[] cost = new int[len0];
		int[] newcost = new int[len0];

		// initial cost of skipping prefix in String s0
		for (int i = 0; i < len0; i++) cost[i] = i;

		// dynamically computing the array of distances

		// transformation cost for each letter in s1
		for (int j = 1; j < len1; j++) {
			// initial cost of skipping prefix in String s1
			newcost[0] = j;

			// transformation cost for each letter in s0
			for(int i = 1; i < len0; i++) {
				// matching current letters in both strings
				int match = (lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1;

				// computing cost for each transformation
				int cost_replace = cost[i - 1] + match;
				int cost_insert  = cost[i] + 1;
				int cost_delete  = newcost[i - 1] + 1;
				// keep minimum cost
				newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
			}
			// swap cost/newcost arrays
			int[] swap = cost; cost = newcost; newcost = swap;
		}
		// the distance is the cost for transforming all letters in both strings
		return cost[len0 - 1];
	}
}

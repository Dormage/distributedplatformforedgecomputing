package helpers;

import java.net.*;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.*;
import application.Application;
import common.Node;
import configs.NetworkConfig;
import configs.NodeConfig;

public class NetworkUtil {
	private static final int NODE_ID_BITS = 10;
	private static final int maxNodeId = (int) (Math.pow(2, NODE_ID_BITS) - 1);

	/*
	 * Various provides are used to try and discover the external IP addres of the
	 * node. The address is then broadcasted to all new nodes in the peer discovery
	 * protocol. This should only be ran in production evniorment
	 */
	public static String discoverExternalIP() {
		if (NetworkConfig.isProduction) {
			// request @ 91.198.22.70 on port 80
			// request @ 74.208.43.192 on port 80
			// request @ www.showmyip.com
			return null;
		} else if(NodeConfig.running_docker){
			try {
				return InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException e) {
				System.out.println(Constants.ERROR + "Unable to get local IP address");
			}
		}else if(NodeConfig.running_host){
			String ip="";
			try {
				Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
				while (interfaces.hasMoreElements()) {
					NetworkInterface iface = interfaces.nextElement();
					// filters out 127.0.0.1 and inactive interfaces
					if (iface.isLoopback() || !iface.isUp()) {
						System.out.println("Loopback");
						continue;
					}
					Enumeration<InetAddress> addresses = iface.getInetAddresses();
					while(addresses.hasMoreElements()) {
						InetAddress addr = addresses.nextElement();
						ip = addr.getHostAddress();
					}
				}
			} catch (SocketException e) {
				throw new RuntimeException(e);
			}
			return ip;
		}
		return null;
	}

	public static Node getRandomNode(){
		List<Node> nodes = new ArrayList<Node>(Application.known_nodes.values());
		Random random = new Random();
		return nodes.get(random.nextInt(nodes.size()));
	}
	
	public static String applySha256(String input){		
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");	        
			//Applies sha256 to our input, 
			byte[] hash = digest.digest(input.getBytes("UTF-8"));	        
			StringBuffer hexString = new StringBuffer(); // This will contain hash as hexidecimal
			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);
			}
			return hexString.toString();
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
} 

	public static String createNodeId() {
		int nodeId;
		try {
			StringBuilder sb = new StringBuilder();
			Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
			while (networkInterfaces.hasMoreElements()) {
				NetworkInterface networkInterface = networkInterfaces.nextElement();
				byte[] mac = networkInterface.getHardwareAddress();
				if (mac != null) {
					for (int i = 0; i < mac.length; i++) {
						sb.append(String.format("%02X", mac[i]));
					}
				}
			}
			nodeId = sb.toString().hashCode();
		} catch (Exception ex) {
			nodeId = (new SecureRandom().nextInt());
		}
		nodeId = nodeId & maxNodeId;
		return applySha256(""+nodeId);
	}
	
	//broadcasts a message to all clients connected including self
	public static void broadcastToAll(protobuf.ProtoMessage.Message m) {
		Application.known_nodes.forEach((key, value)-> value.send(m));
	}
	
	public static void printNodes() {
		for (Node node : Application.known_nodes.values()) {
			printNode(node);
		}
	}
	public static void printNode(Node node) {
		System.out.println(Constants.NETWORK + "ID: " + node.getId());
	}
}

package helpers;

import application.Application;
import common.Block;
import services.Lottery_draw;

public class LotteryHelper {

    public static Lottery_draw lottery_draw=null;

    public static void drawLottery(Block block){
        if(lottery_draw == null) {
            lottery_draw = new Lottery_draw(block);
        } else if (lottery_draw.isRunning()) {
            lottery_draw.Destroy();
            lottery_draw = new Lottery_draw(block);
        } else {
            lottery_draw = new Lottery_draw(block);
        }
        Application.executor.execute(lottery_draw);
    }

    public static boolean isRunning(){
        if(lottery_draw == null) {
            return false;
        } else {
            return  lottery_draw.isRunning();
        }
    }
    public static boolean stop(){
        return lottery_draw.Destroy();
    }
}

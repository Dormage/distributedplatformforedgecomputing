package helpers;

import common.App;
import common.Block;
import common.MessageModel;
import common.Stat;
import protobuf.ProtoMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProtobufMessageHelper {

    public static ProtoMessage.Message writeMessageWithStringArray(ProtoMessage.Message.Protocol protocol, ProtoMessage.Message.Type type, String[] arr) {
        ProtoMessage.Message.Builder myMessage = ProtoMessage.Message.newBuilder();
        myMessage.setProtocol(protocol);
        myMessage.setType(type);
        ProtoMessage.Message.Payload.Builder payload = ProtoMessage.Message.Payload.newBuilder();
        for (String s : arr) {
            payload.addNode(s);
        }
        myMessage.setPayload(payload);
        return myMessage.build();
    }

    public static ProtoMessage.Message writeMessageWithBlockHeight(ProtoMessage.Message.Protocol protocol, ProtoMessage.Message.Type type, int height) {
        ProtoMessage.Message.Builder myMessage = ProtoMessage.Message.newBuilder();
        myMessage.setProtocol(protocol);
        myMessage.setType(type);
        ProtoMessage.Message.Payload.Builder payload = ProtoMessage.Message.Payload.newBuilder();
        payload.setHeight(height);
        myMessage.setPayload(payload);
        return myMessage.build();
    }

    public static ProtoMessage.Message writeMessageWithBlocks(ProtoMessage.Message.Protocol protocol, ProtoMessage.Message.Type type, Block[] blocks) {
        ProtoMessage.Message.Builder message = ProtoMessage.Message.newBuilder();
        message.setProtocol(protocol);
        message.setType(type);
        message.setPayload(writeBlocks(blocks));
        return message.build();
    }

    public static MessageModel readData(ProtoMessage.Message message) {
        switch (message.getProtocol()) {
            case BLOCK_PROPAGATION_PROTOCOL:
                break;
            case HANDSHAKE_PROTOCOL:
                break;
            case PEER_DISCOVERY_PROTOCOL:
                break;
            case BLOCKCHAIN_SYNC_PROTOCOL:
                break;
            case RESOURCE_PROPAGATION_PROTOCOL:
                break;
            case BIZANTINE_FAULT_TOLERANCE_PROTOCOL:
                break;
        }
        switch (message.getType()) {
            case REQUEST:
                break;
            case RESPONSE:
                break;
            case NEW_BLOCK:
                break;
        }
        return new MessageModel(message.getProtocol(), message.getType(), message.getPayload());
    }

    public static ProtoMessage.Message.Payload writeBlocks(Block[] blocks) {
        ProtoMessage.Block.Builder blockBuilder = ProtoMessage.Block.newBuilder();

        ProtoMessage.Message.Payload.Builder blockList = ProtoMessage.Message.Payload.newBuilder();

        for (Block block : blocks) {
            ProtoMessage.Block.MapFieldEntry.Builder mapBuilder = ProtoMessage.Block.MapFieldEntry.newBuilder();

            blockBuilder.setBlockHash(block.getBlock_hash());
            blockBuilder.setPrevHash(block.getPrevious_block_hash());
            blockBuilder.setId(block.getId());
            blockBuilder.setTicket(block.getTicket());
            blockBuilder.setDifficulty(block.getDifficulty());
            blockBuilder.setVdfProof(block.getVdf_proof());
            blockBuilder.setBlockProducer(block.getBlock_producer());
            blockBuilder.setTimestamp(block.getTimestamp());
            blockBuilder.setFinal(block.getBlock_final());

            if (block.data != null) {

                for (Map.Entry<String, Stat> entry : block.data.entrySet()) {
                    ProtoMessage.Stat.Builder statBuilder = ProtoMessage.Stat.newBuilder();

                    mapBuilder.setKey(entry.getKey());
                    statBuilder.setId(entry.getValue().getId());
                    statBuilder.setCpu(entry.getValue().getCpu());
                    statBuilder.setRam(entry.getValue().getRam());
                    statBuilder.setDisk(entry.getValue().getDisk());
                    statBuilder.setNodeId(entry.getValue().getNode_id());
                    statBuilder.setTimestamp(entry.getValue().getTimestamp());

                    mapBuilder.setValue(statBuilder.build());
                    blockBuilder.addData(mapBuilder.build());
                }
            }
            blockList.addBlock(blockBuilder.build());
        }
        return blockList.build();
    }

    public static Block[] readBlocks(ProtoMessage.Message.Payload payload) {
        List<Block> blocks = new ArrayList<>();
        HashMap<String, Stat> data = new HashMap<>();

        for (ProtoMessage.Block block : payload.getBlockList()) {
            Block newBlock = new Block();
            newBlock.setBlock_hash(block.getBlockHash());
            newBlock.setPrevious_block_hash(block.getPrevHash());
            newBlock.setId(block.getId());
            newBlock.setTicket(block.getTicket());
            newBlock.setDifficulty(block.getDifficulty());
            newBlock.setVdf_proof(block.getVdfProof());
            newBlock.setBlock_producer(block.getBlockProducer());
            newBlock.setTimestamp(block.getTimestamp());
            newBlock.setBlock_final(block.getFinal());

            if (!block.getDataList().isEmpty()) {
                for (int i = 0; i < block.getDataList().size(); i++) {

                    if (block.getData(i).hasValue()) {
                        Stat stat = new Stat();
                        stat.setId(block.getData(i).getValue().getId());
                        stat.setCpu(block.getData(i).getValue().getCpu());
                        stat.setNode_id(block.getData(i).getValue().getNodeId());
                        stat.setTimestamp(block.getData(i).getValue().getTimestamp());
                        data.put(block.getData(i).getKey(), stat);
                    }
                }
                if (!data.isEmpty()) {
                    newBlock.data = data;
                }
            }
            blocks.add(newBlock);
        }
        System.gc();
        return blocks.toArray(new Block[0]);
    }

    public static ProtoMessage.Message writeMessageWithAppsAndStats(ProtoMessage.Message.Protocol protocol, Stat[] stats, App[] apps) {

        ProtoMessage.Message.Builder myMessage = ProtoMessage.Message.newBuilder();

        myMessage.setProtocol(protocol);

        ProtoMessage.Message.Payload.Builder payload = ProtoMessage.Message.Payload.newBuilder();

        for (Stat stat : stats) {
            ProtoMessage.Stat.Builder statBuilder = ProtoMessage.Stat.newBuilder();
            statBuilder.setId(stat.getId());
            statBuilder.setCpu(stat.getCpu());
            statBuilder.setRam(stat.getRam());
            statBuilder.setDisk(stat.getDisk());
            statBuilder.setTimestamp(stat.getTimestamp());
            statBuilder.setNodeId(stat.getNode_id());
            payload.addStat(statBuilder.build());
        }

        for (App app : apps) {
            ProtoMessage.App.Builder appBuilder = ProtoMessage.App.newBuilder();
            appBuilder.setId(app.getId());
            appBuilder.setDid(app.getDid());
            appBuilder.setName(app.getName());
            payload.addApp(appBuilder.build());
        }
        myMessage.setPayload(payload);
        return myMessage.build();
    }

    public static Stat[] readMessageWithStats(ProtoMessage.Message message) {
        List<Stat> stats = new ArrayList<>();

        if (message.getPayload().getStatList().size() > 0) {
            for (ProtoMessage.Stat stat : message.getPayload().getStatList()) {
                Stat newStat = new Stat();
                newStat.setId(stat.getId());
                newStat.setCpu(stat.getCpu());
                newStat.setRam(stat.getRam());
                newStat.setDisk(stat.getDisk());
                newStat.setTimestamp(stat.getTimestamp());
                newStat.setNode_id(stat.getNodeId());
                stats.add(newStat);
            }
        }
        return stats.toArray(new Stat[0]);
    }

    public static App[] readMessageWithApps(ProtoMessage.Message message) {
        List<App> apps = new ArrayList<>();

        for (ProtoMessage.App app : message.getPayload().getAppList()) {
            App newApp = new App();

            newApp.setId(app.getId());
            newApp.setDid(app.getDid());
            newApp.setName(app.getName());

            apps.add(newApp);
        }
        return apps.toArray(new App[0]);
    }
}

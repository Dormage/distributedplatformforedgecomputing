package application;

import common.Block;
import common.Node;
import common.NodeModel;
import common.Stat;
import configs.NetworkConfig;
import helpers.*;
import influxDB.Influx;
import protocols.BlockchainSyncProtocol;
import services.*;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


public class Application {

	public static ConcurrentHashMap<String, Node> known_nodes = new ConcurrentHashMap();
	public static ServerSocket serverSocket;
	public static ExecutorService executor = Executors.newCachedThreadPool();
	public static AtomicBoolean chain_synced = new AtomicBoolean(false);
	public static Influx influx;
	public static Map<String, Long> messageQueue = new ConcurrentHashMap<>();
	public static Logger.Level[] logConfig = {Logger.Level.ERROR, Logger.Level.INFO, Logger.Level.BLOCKCHAIN, Logger.Level.NETWORK,
			Logger.Level.DEBUG, Logger.Level.FORK, Logger.Level.WARNING};
	public static AtomicInteger noOfMessagesReceived = new AtomicInteger(0);
	public static AtomicInteger noOfMessagesSend = new AtomicInteger(0);
	public static AtomicInteger sizeOfMessages = new AtomicInteger(0);
	public static AtomicInteger incommingBlock = new AtomicInteger(0);
	public static Block lastCandidate = new Block("", 0, Integer.MAX_VALUE, 15000, "", "origin_block", new HashMap<String, Stat>(), true);;

	public static void main(String[] args) {

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				Logger.log(Constants.NETWORK + "Messages received:" + noOfMessagesReceived + " Messages sent: " + noOfMessagesSend + " Total sent: " +sizeOfMessages + " bytes", Logger.Level.NETWORK);
				noOfMessagesReceived.set(0);
				noOfMessagesSend.set(0);
				sizeOfMessages.set(0);
			}
		}, 60000, 60000);

		if (args.length > 0) {
			List<Logger.Level> levels = new ArrayList<>();
			for (String arg : args) {
				if (arg.equals("all")) {
					levels.add(Logger.Level.ERROR);
					levels.add(Logger.Level.INFO);
					levels.add(Logger.Level.BLOCKCHAIN);
					levels.add(Logger.Level.NETWORK);
					levels.add(Logger.Level.DEBUG);
					levels.add(Logger.Level.FORK);
					levels.add(Logger.Level.WARNING);
				} else if (arg.equals("error")) {
					levels.add(Logger.Level.ERROR);
				} else if (arg.equals("warning")) {
					levels.add(Logger.Level.WARNING);
				} else if (arg.equals("info")) {
					levels.add(Logger.Level.INFO);
				} else if (arg.equals("debug")) {
					levels.add(Logger.Level.DEBUG);
				} else if (arg.equals("fork")) {
					levels.add(Logger.Level.FORK);
				} else if (arg.equals("bc")) {
					levels.add(Logger.Level.BLOCKCHAIN);
				} else if (arg.equals("network")) {
					levels.add(Logger.Level.NETWORK);
				} else if (arg.equals("transfer")) {
					levels.add(Logger.Level.TRANSFER);
				}
			}
			logConfig = levels.toArray(new Logger.Level[0]);
		}

		DatabaseHelper.initDB("database");

		// monitor user input
		executor.execute(new InputCommandHandler());
		// initialize docker client API
		DockerUtil.init();
		// initialize file transfer service
		executor.execute(new FileTransferService());
		// initialize resource monitoring service
		executor.execute(new ContainerStatsService(NetworkConfig.resource_propagation_time));

		influx = new Influx("http://88.200.63.190:8086", "edge","e1d2g3e4");

		// discover my own IP, works for LAN in development mode.
		NetworkConfig.local_ip = NetworkUtil.discoverExternalIP();
		// generate a unique id using network interfaces as a seed
		NetworkConfig.node_id = NetworkUtil.createNodeId();

		Logger.log(Constants.NETWORK + "Created node id " + NetworkConfig.node_id + " IP: " + NetworkConfig.local_ip, Logger.Level.NETWORK);
		new WebServer();
		new SensorServer();

		if (DatabaseHelper.getAllBlocks().length == 0) {
			Block origin = new Block("", 0, Integer.MAX_VALUE, 15000, "", "origin_block", new HashMap<String, Stat>(), true);
			DatabaseHelper.addBlock(origin);
		}

		NodeModel[] nodes = DatabaseHelper.getAllKnownNodes();
		if (nodes.length > 0) {
			System.out.println(Constants.SYNC + "Reconnecting...");
			for (NodeModel node : nodes) {
				if (!Application.known_nodes.containsKey(node.getId()) && !node.getIp().equals(NetworkConfig.local_ip)) { //unknown node
					Node n = new Node(node.getIp(), node.getPort(), node.getId());
					if (n.connect()) {
						Application.executor.execute(n);
					} else {
						Logger.log(Constants.NETWORK + "Connection faiiled! " + node.getId() + " reason: " + n.toString(), Logger.Level.INFO);
					}
				} else {
					Logger.log(Constants.NETWORK + "node "  + node.getId() + " already known", Logger.Level.NETWORK);
				}
			}
		} else if (!NetworkConfig.trusted_node_ip.equals(NetworkConfig.local_ip)) {
			Logger.log(Constants.NETWORK + "Trying to connect to trusted node.", Logger.Level.NETWORK);
			Node trustedNode = new Node(NetworkConfig.trusted_node_ip, NetworkConfig.trusted_node_port);
			trustedNode.connect();
			executor.execute(trustedNode);
			BlockchainSyncProtocol.requestBlocks(trustedNode);
		} else {
			chain_synced.set(true);
			LotteryHelper.drawLottery(DatabaseHelper.getLastBlock());
		}
		executor.execute(new PeerDiscoveryService());

		while (true) {
			try {
				if (serverSocket == null) {
					serverSocket = new ServerSocket(NetworkConfig.server_port);
					Logger.log(Constants.NETWORK + "Started listening on port " + NetworkConfig.server_port, Logger.Level.NETWORK);
				}
			} catch (IOException e) {
				Logger.log(Constants.NETWORK + "Failed to acquire a socket: " + NetworkConfig.server_port, Logger.Level.ERROR);
				e.printStackTrace();
			}
			try {
				Socket client = serverSocket.accept();
				Node n = new Node(client.getInetAddress().getHostAddress(), client.getPort(), client);
				executor.execute(n);
	
				Logger.log(Constants.NETWORK + "Accepted socket connection from " + client.getInetAddress() + ":" + client.getPort(), Logger.Level.INFO);
				System.out.println(Constants.NETWORK + "Established connection with new node " + n.getId());
				Logger.log(Constants.NETWORK + "Established connection with new node " + n.getId(), Logger.Level.NETWORK);
			} catch (IOException e) {
				Logger.log(Constants.NETWORK + "Client failed to connect", Logger.Level.ERROR);
				e.printStackTrace();
			}
		}
	}
}

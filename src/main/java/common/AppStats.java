package common;

public class AppStats {
    double CPU;
    double MEM;
    double IO;
    double NET;

    public AppStats(double CPU, double MEM, double IO, double NET) {
        this.NET = NET;
        this.CPU = CPU;
        this.IO = IO;
        this.MEM =MEM;
    }

    public double getCPU() {
        return CPU;
    }

    public void setCPU(double CPU) {
        this.CPU = CPU;
    }

    public double getMEM() {
        return MEM;
    }

    public void setMEM(double MEM) {
        this.MEM = MEM;
    }

    public double getIO() {
        return IO;
    }

    public void setIO(double IO) {
        this.IO = IO;
    }

    public double getNET() {
        return NET;
    }

    public void setNET(double NET) {
        this.NET = NET;
    }
    public String toString(){
        return this.CPU+","+this.MEM +","+this.IO +","+this.NET;
    }
}

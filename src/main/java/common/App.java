package common;

/*Application abstraction implementing docker container functionality
*/
public class App {
	String id; // unique ID across nodes
	String did; // docker container ID
	String name; // container name
	/* Stats in % obtained from docker stats */
	int cpu;
	int memory;
	int net;
	int disk;
	public App(String id, String did, String name) {
		super();
		this.id = id;
		this.did = did;
		this.name = name;
	}

	public App() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDid() {
		return did;
	}

	public void setDid(String did) {
		this.did = did;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCpu() {
		return cpu;
	}

	public void setCpu(int cpu) {
		this.cpu = cpu;
	}

	public int getMemory() {
		return memory;
	}

	public void setMemory(int memory) {
		this.memory = memory;
	}

	public int getNet() {
		return net;
	}

	public void setNet(int net) {
		this.net = net;
	}

	public int getDisk() {
		return disk;
	}

	public void setDisk(int disk) {
		this.disk = disk;
	}

}

package common;

public class BlockEntry {
	String app_id;
	String node_id;
	float resource;
	long timestamp;

	public BlockEntry(String app_id, String node_id, float resource, long timestamp) {
		this.app_id = app_id;
		this.node_id = node_id;
		this.resource = resource;
		this.timestamp = timestamp;
	}
}

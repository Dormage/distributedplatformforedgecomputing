package common;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

import application.Application;
import configs.NetworkConfig;
import helpers.Constants;
import helpers.DatabaseHelper;
import helpers.DockerUtil;
import helpers.Logger;

public class FileSender extends Thread {
	Socket connection;
	BufferedReader in;
	BufferedWriter out;
	String URI;
	String address;
	int port;
	String container_id;
	String nodeID;
	int blockID;
	String blockProducer;

	public FileSender(String URI, String address, int port, String container_id, String nodeID, int blockID, String blockProducer) {
		this.URI = URI;
		this.address = address;
		this.port = port;
		this.container_id = container_id;
		this.nodeID = nodeID;
		this.blockID = blockID;
		this.blockProducer = blockProducer;
	}

	public void run() {
		try {
			Logger.log("Connecting to  " + address + " :  " + port, Logger.Level.NETWORK);
			connection = new Socket(address, port);
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			out = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
			sendFile(URI, container_id);
		} catch (IOException e) {
			Logger.log(Constants.ERROR + "Filed to initialize file transfer..." + e.getMessage(), Logger.Level.ERROR);
		}
	}

	public void sendFile(String URI, String container_id) {
		File file = new File(URI);
		FileDetails details;
		byte[] data;
		data = new byte[2048]; // Here you can increase the size also which will send it faster
		details = new FileDetails();

		NodeModel[] nodes = DatabaseHelper.getAllKnownNodes();
		AppsWithStats appsWithStats = DatabaseHelper.getApps();

		AppsWithStats senderStats = DatabaseHelper.getAppByNodeID(NetworkConfig.node_id);
		AppsWithStats receiverStats = DatabaseHelper.getAppByNodeID(nodeID);

		double senderCPU = 0.0;
		for (Stat stat : senderStats.stats) {
			senderCPU += stat.cpu;
		}
		Logger.log(Constants.DOCKER + "Influx: " + blockID + " " + NetworkConfig.node_id + " " + senderCPU, Logger.Level.TRANSFER);
		Application.influx.newNodeResources(blockID, NetworkConfig.node_id, senderCPU);

		double receiverCPU = 0.0;
		for (Stat stat : receiverStats.stats) {
			receiverCPU += stat.cpu;
		}

		details.setDetails(file.getName(), file.length(), container_id, NetworkConfig.node_id, nodeID, senderCPU, receiverCPU, blockID, blockProducer);

		for (NodeModel nodeModel : nodes) {
			double cpu = 0.0;
			for (int i = 0; i < appsWithStats.apps.length; i++) {
				if (appsWithStats.stats[i].getNode_id().equals(nodeModel.id)) {
					cpu += appsWithStats.stats[i].getCpu();
				}
			}
			Logger.log(Constants.DOCKER + "Influx: " + blockID + " " + nodeModel.id + " " + cpu, Logger.Level.TRANSFER);
			Application.influx.newNodeResources(blockID, nodeModel.id, cpu);
		}

		DatabaseHelper.deleteApp(container_id);

		try {
			ObjectOutputStream sendDetails = new ObjectOutputStream(connection.getOutputStream());
			sendDetails.writeObject(details);
			sendDetails.flush();
			// Sending File Data
			System.out.println("Sending file data...");
			Logger.log("Sending file data...", Logger.Level.INFO);
			FileInputStream fileStream = new FileInputStream(file);
			BufferedInputStream fileBuffer = new BufferedInputStream(fileStream);
			OutputStream out = connection.getOutputStream();
			int count;
			while ((count = fileBuffer.read(data)) > 0) {
				//System.out.println("Data Sent : " + count);
				out.write(data, 0, count);
				out.flush();
			}
			out.close();
			fileBuffer.close();
			fileStream.close();

			//cleanup
			DockerUtil.removeCompressedContainer(URI);
		} catch (Exception e) {
			Logger.log("Error : " + e.toString(), Logger.Level.INFO);
		}
	}
}

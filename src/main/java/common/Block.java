package common;

import java.util.HashMap;

import helpers.CryptoUtil;

public class Block {
	String block_hash;
	String previous_block_hash;
	int id;
	int ticket;
	int difficulty;
	String vdf_proof;
	String block_producer;
	Long timestamp;
	Boolean block_final;

	public HashMap<String, Stat> data;

	public Block(String previous_block_hash, int id, int ticket, int difficulty, String vdf_proof, String block_producer, HashMap<String, Stat> data, Boolean block_final) {
		this.block_hash = CryptoUtil.Hash(previous_block_hash + data.hashCode() + ticket);
		this.previous_block_hash = previous_block_hash;
		this.id = id;
		this.ticket = ticket;
		this.difficulty = difficulty;
		this.vdf_proof = vdf_proof;
		this.data = data;
		this.block_producer = block_producer;
		this.timestamp = System.currentTimeMillis();
		this.block_final = block_final;
	}

	public Block(String previous_block_hash, int id, int ticket, int difficulty, String vdf_proof, String block_producer, Long timestamp, HashMap<String, Stat> data, Boolean block_final) {
		this.block_hash = CryptoUtil.Hash(previous_block_hash + data.hashCode() + ticket);
		this.previous_block_hash = previous_block_hash;
		this.id = id;
		this.ticket = ticket;
		this.difficulty = difficulty;
		this.vdf_proof = vdf_proof;
		this.data = data;
		this.block_producer = block_producer;
		this.timestamp = timestamp;
		this.block_final = block_final;
	}

	public Block(String block_hash, String previous_block_hash, int id, int ticket, int difficulty, String vdf_proof, String block_producer, Long timestamp, HashMap<String, Stat> data, Boolean block_final) {
		this.block_hash = block_hash;
		this.previous_block_hash = previous_block_hash;
		this.id = id;
		this.ticket = ticket;
		this.difficulty = difficulty;
		this.vdf_proof = vdf_proof;
		this.data = data;
		this.block_producer = block_producer;
		this.timestamp = timestamp;
		this.block_final = block_final;
	}

	public Block() {

	}

	public String getBlock_hash() {
		return block_hash;
	}

	public int getTicket() {
		return ticket;
	}

	public void setTicket(int ticket) {
		this.ticket = ticket;
	}

	public int getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	public String getVdf_proof() {
		return vdf_proof;
	}

	public void setVdf_proof(String vdf_proof) {
		this.vdf_proof = vdf_proof;
	}

	public void setBlock_hash(String block_hash) {

		this.block_hash = block_hash;
	}

	public String getPrevious_block_hash() {
		return previous_block_hash;
	}

	public void setPrevious_block_hash(String previous_block_hash) {
		this.previous_block_hash = previous_block_hash;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBlock_producer() {
		return block_producer;
	}

	public void setBlock_producer(String block_producer) {
		this.block_producer = block_producer;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Boolean getBlock_final() {
		return block_final;
	}

	public void setBlock_final(Boolean block_final) {
		this.block_final = block_final;
	}
}

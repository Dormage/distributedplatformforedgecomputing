package common;

import java.io.Serializable;

public class FileDetails implements Serializable {

	String name;
	long size;
	String app_id;
	String senderID;
	String receiverID;
	double senderCPU;
	double receiverCPU;
	int blockID;
	String blockProducer;

	public void setDetails(String name, long size, String container_id, String senderID, String receiverID, double senderCPU, double receiverCPU, int blockID, String blockProducer) {
		this.name = name;
		this.size = size;
		this.app_id = container_id;
		this.senderID = senderID;
		this.receiverID = receiverID;
		this.senderCPU = senderCPU;
		this.receiverCPU = receiverCPU;
		this.blockID = blockID;
		this.blockProducer = blockProducer;
	}

	public String getName() {
		return name;
	}

	public long getSize() {
		return size;
	}

	public String getContainer_id() {
		return app_id;
	}

	public void setContainer_id(String container_id) {
		this.app_id = container_id;
	}

	public String getSenderID() {
		return senderID;
	}

	public void setSenderID(String senderID) {
		this.senderID = senderID;
	}

	public String getReceiverID() {
		return receiverID;
	}

	public void setReceiverID(String receiverID) {
		this.receiverID = receiverID;
	}

	public double getSenderCPU() {
		return senderCPU;
	}

	public void setSenderCPU(double senderCPU) {
		this.senderCPU = senderCPU;
	}

	public double getReceiverCPU() {
		return receiverCPU;
	}

	public void setReceiverCPU(double receiverCPU) {
		this.receiverCPU = receiverCPU;
	}

	public int getBlockID() {
		return blockID;
	}

	public void setBlockID(int blockID) {
		this.blockID = blockID;
	}

	public String getBlockProducer() {
		return blockProducer;
	}

	public void setBlockProducer(String blockProducer) {
		this.blockProducer = blockProducer;
	}
}
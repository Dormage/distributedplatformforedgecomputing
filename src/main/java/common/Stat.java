package common;

import helpers.Constants;

public class Stat {
	String id;
	double cpu;
	double ram;
	double disk;
	String node_id;
	long timestamp;

	public Stat(String id, double cpu, double ram, double disk, long timestamp, String node_id) {
		this.id = id;
		this.cpu = cpu;
		this.ram = ram;
		this.disk = disk;
		this.timestamp = timestamp;
		this.node_id = node_id;
	}

    public Stat() {

    }

    public String getId() {
		return id;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getCpu() {
		return cpu;
	}

	public void setCpu(double cpu) {
		this.cpu = cpu;
	}

	public String getNode_id() {
		return node_id;
	}

	public void setNode_id(String node_id) {
		this.node_id = node_id;
	}
	public void print() {
		System.out.println(Constants.RESOURCE + "AppId: " + id + " on Node: " + node_id + " using: " + cpu + " at: "+ timestamp/1000L);
	}

	public double getRam() {
		return ram;
	}

	public void setRam(double ram) {
		this.ram = ram;
	}

	public double getDisk() {
		return disk;
	}

	public void setDisk(double disk) {
		this.disk = disk;
	}
}
 
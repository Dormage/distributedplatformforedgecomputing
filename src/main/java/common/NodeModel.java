package common;

import java.sql.Timestamp;

public class NodeModel {
    String id;
    String ip;
    int port;
    Timestamp ping;

    public NodeModel(String id, String ip, int port, Timestamp ping) {
        this.id = id;
        this.ip = ip;
        this.port = port;
        this.ping = ping;
    }

    public String getId() {
        return id;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public Timestamp getPing() {
        return ping;
    }
}

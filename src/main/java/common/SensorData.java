package common;

import java.util.Map;

public class SensorData {
    Map<String, String> meta;
//    String sn;
//    String fw;
//    String ip;
//    String name;
//    String desc;
//    String uptime;
    double T;
    double RH;
    double CO2;
    double p;
    double ambient_light;
    double VOC_index;
    double VOC_equiv_CO2;
    double VOC_accuracy;
    double PM2_5;
    double PM10;

    public Map<String, String> getMeta() {
        return meta;
    }

    public void setMeta(Map<String, String> meta) {
        this.meta = meta;
    }

    public double getT() {
        return T;
    }

    public void setT(double t) {
        T = t;
    }

    public double getRH() {
        return RH;
    }

    public void setRH(double RH) {
        this.RH = RH;
    }

    public double getCO2() {
        return CO2;
    }

    public void setCO2(double CO2) {
        this.CO2 = CO2;
    }

    public double getP() {
        return p;
    }

    public void setP(double p) {
        this.p = p;
    }

    public double getAmbient_light() {
        return ambient_light;
    }

    public void setAmbient_light(double ambient_light) {
        this.ambient_light = ambient_light;
    }

    public double getVOC_index() {
        return VOC_index;
    }

    public void setVOC_index(double VOC_index) {
        this.VOC_index = VOC_index;
    }

    public double getVOC_equiv_CO2() {
        return VOC_equiv_CO2;
    }

    public void setVOC_equiv_CO2(double VOC_equiv_CO2) {
        this.VOC_equiv_CO2 = VOC_equiv_CO2;
    }

    public double getVOC_accuracy() {
        return VOC_accuracy;
    }

    public void setVOC_accuracy(double VOC_accuracy) {
        this.VOC_accuracy = VOC_accuracy;
    }

    public double getPM2_5() {
        return PM2_5;
    }

    public void setPM2_5(double PM2_5) {
        this.PM2_5 = PM2_5;
    }

    public double getPM10() {
        return PM10;
    }

    public void setPM10(double PM10) {
        this.PM10 = PM10;
    }
}

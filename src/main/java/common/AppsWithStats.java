package common;

public class AppsWithStats {
    public App[] apps;
    public Stat[] stats;

    public AppsWithStats(App[] apps, Stat[] stats) {
        this.apps = apps;
        this.stats = stats;
    }
}

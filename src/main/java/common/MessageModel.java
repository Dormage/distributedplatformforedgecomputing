package common;


public class MessageModel {

    public protobuf.ProtoMessage.Message.Protocol protocol; // type of protocol
    public protobuf.ProtoMessage.Message.Type type;
    public protobuf.ProtoMessage.Message.Payload content; // payload

    public MessageModel(
            protobuf.ProtoMessage.Message.Protocol protocol,
            protobuf.ProtoMessage.Message.Type type,
            protobuf.ProtoMessage.Message.Payload content
    ) {
        this.protocol = protocol;
        this.type = type;
        this.content = content;
    }

    public String toString() {
        return "Protocol: " + protocol + " | content: " + content+ " | type: " + type;
    }
}

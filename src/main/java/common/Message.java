package common;
import helpers.CryptoUtil;

public class Message {

	public String header; // type of message
	public String content; // json encoded payload
	public String type;
	public String signature;

	public Message(String header, String content, String type) {
		this.header = header;
		this.content = content;
		this.type = type;
		this.signature = CryptoUtil.Hash(content);
	}
	
	public String toString() {
		return "Header: " + header + " | content: " + content+ " | type: " + type + " | Hash: " + signature.substring(5);
	}
}

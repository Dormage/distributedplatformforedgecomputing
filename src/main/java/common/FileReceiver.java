package common;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import application.Application;
import configs.NetworkConfig;
import helpers.Constants;
import helpers.DatabaseHelper;
import helpers.DockerUtil;
import helpers.Logger;

public class FileReceiver extends Thread {
	Socket connection;
	BufferedReader in;
	BufferedWriter out;

	public FileReceiver(Socket connection) throws IOException {
		this.connection = connection;
		in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		out = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
	}

	public void run() {
		ObjectInputStream getDetails;
		try {
			getDetails = new ObjectInputStream(connection.getInputStream());
			FileDetails details = (FileDetails) getDetails.readObject();
			String fileName = details.getName();
			byte data[] = new byte[2048]; // Here you can increase the size also which will receive it faster
			FileOutputStream fileOut = new FileOutputStream(fileName);
			InputStream fileIn = connection.getInputStream();
			BufferedOutputStream fileBuffer = new BufferedOutputStream(fileOut);
			int count;
			int sum = 0;
			long start_time = System.currentTimeMillis();
			while ((count = fileIn.read(data)) > 0) {
				sum += count;
				fileBuffer.write(data, 0, count);
				// System.out.println("Data received : " + sum);
				fileBuffer.flush();
			}
			Logger.log(Constants.DOCKER + "File Received: " + details.name, Logger.Level.TRANSFER);
			String container_id = DockerUtil.loadImage(details.name, details.app_id);
			String newDID = DockerUtil.startTransferedContainer(container_id);
			Logger.log(Constants.DOCKER+"App  "+ details.app_id + " loaded as container " + container_id, Logger.Level.TRANSFER);
			Logger.log(Constants.DOCKER+"App  "+ details.app_id + " started with DID " + newDID, Logger.Level.TRANSFER);
            DatabaseHelper.addApp(details.app_id, newDID, details.app_id, 0, 0, 0, 0, NetworkConfig.node_id);

			fileBuffer.close();
			fileIn.close();
			long duration = System.currentTimeMillis()-start_time;
			//CLEANUP
			DockerUtil.removeCompressedContainer(details.name);
            Application.influx.newTransfer(details, duration);

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			Logger.log(Constants.ERROR + "Failed resuming transfered container...", Logger.Level.ERROR);
			/*Skipped confirmation protocol assuming no faults in test setting*/
			e.printStackTrace();
		}
	}
}

package common;

import java.io.*;
import java.net.Socket;
import java.sql.Timestamp;
import application.Application;
import configs.NetworkConfig;
import helpers.*;
import protobuf.ProtoMessage;
import protocols.*;

public class Node implements Runnable {
	String id;
	private String address;
	private int port;
	Socket connection;
	InputStream in;
	OutputStream out;
	private long last_update;
	private long timeout = 5000;
	long timestamp;
	boolean handshake = false;
	int currentBlockHeight = 0;
	double random_seed;

	public int getCurrentBlockHeight() {
		return currentBlockHeight;
	}

	public void setCurrentBlockHeight(int currentBlockHeight) {
		this.currentBlockHeight = currentBlockHeight;
	}

	public boolean isHandshake() {
		return handshake;
	}

	public void setHandshake(boolean handshake) {
		this.handshake = handshake;
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public long getLast_update() {
		return last_update;
	}

	public void setLast_update(long last_update) {
		this.last_update = last_update;
	}

	public Node(String address, String port) {
		this.address = address;
		this.port = Integer.parseInt(port);
		this.random_seed = Math.random();
	}

	public Node(String address, int port) {
		this.address = address;
		this.port = port;
		this.random_seed = Math.random();
	}

	public Node(String address, int port, String id) {
		this.address = address;
		this.port = port;
		this.id = id;
		this.random_seed = Math.random();
	}

	public Node(String address, int port, Socket connection) throws IOException {
		this.address = address;
		this.port = port;
		this.connection = connection;
		this.random_seed = Math.random();
		in = connection.getInputStream();
		out = connection.getOutputStream();

		this.last_update = System.currentTimeMillis();
		HandshakeProtocol.sendHandshake(this);
	}

	public boolean connect() {
		if (port != 0 && address != null) {
			try {
				connection = new Socket(address, port);

				connection.setReceiveBufferSize(64000);
				connection.setSendBufferSize(64000);
				connection.setKeepAlive(true);
				in = connection.getInputStream();
				out = connection.getOutputStream();

				HandshakeProtocol.sendHandshake(this);
				this.last_update = System.currentTimeMillis();
				return true;
			} catch (IOException e) {
				Logger.log(Constants.NETWORK + "Cant open, closing connection: " +e.getMessage(), Logger.Level.ERROR);
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public void run() {
		Logger.log(Constants.NETWORK + "Listener thread started...", Logger.Level.INFO);
		ProtoMessage.Message message;
		MessageModel messageModel;

		try {
			if (connection != null) {
				while (connection.isConnected()) {
					message = ProtoMessage.Message.parseDelimitedFrom(in);
					if (message != null) {
						messageModel = ProtobufMessageHelper.readData(message);

						Application.noOfMessagesReceived.incrementAndGet();

						if (messageModel.protocol != null) {

							Application.influx.newMessage(getId() != null ? getId() : "unknown", NetworkConfig.node_id, messageModel.protocol.toString(), CryptoUtil.Hash(message.toString()), message.getSerializedSize());

							switch (messageModel.protocol) {
								case HANDSHAKE_PROTOCOL:
									HandshakeProtocol.digest(message, this);
//								if (this.id != null && !Application.known_nodes.containsKey(id) && Application.known_nodes.size() <= NetworkConfig.requiered_nodes || NetworkConfig.local_ip.equals(NetworkConfig.trusted_node_ip)) {
									if (this.id != null && !Application.known_nodes.containsKey(id)) {
										Application.known_nodes.put(id, this);
										DatabaseHelper.addNewNode(getId(), getAddress(), getPort(), new Timestamp(timestamp));
//									Application.influx.newPeer(this);
									}
									break;
								case PEER_DISCOVERY_PROTOCOL:
									PeerDiscoveryProtocol.digest(message, this);
									break;
								case BLOCKCHAIN_SYNC_PROTOCOL:
									BlockchainSyncProtocol.digest(message, this);
									break;
								case BLOCK_PROPAGATION_PROTOCOL:
									BlockPropagationProtocol.digest(message, this);
									break;
								case RESOURCE_PROPAGATION_PROTOCOL:
									ResourcePropagationProtocol.digest(message, this);
									break;
								default:
									Logger.log(Constants.ERROR +"Protocol violation from node:"+ Constants.RESET+" " + this.id, Logger.Level.WARNING);
									break;
							}
						}
					}
				}
			}
		} catch (IOException e) {
			Logger.log("Error reading from socket..." + e.getMessage(), Logger.Level.ERROR);
		}
	}
	
	public synchronized void send(ProtoMessage.Message data) {
		if (isConnected()) {
			try {
				data.writeDelimitedTo(out);
				out.flush();
//				System.out.println(Constants.FILETRANSFER + "Flushed: " + data);
				Application.noOfMessagesSend.incrementAndGet();
				Application.sizeOfMessages.addAndGet(data.getSerializedSize());
			} catch (IOException e) {
				Logger.log("Problem sending: " + this.id, Logger.Level.ERROR);
				Logger.log(data.toString(), Logger.Level.ERROR);
				close();
				Logger.log(Constants.NETWORK + "Cant write, closing connection: " + this.id, Logger.Level.ERROR);
			}
		}
	}

	public boolean close() {
		boolean result = true;
		try {
			in.close();
			out.close();
			connection.close();
//			DatabaseHelper.closeConnections();
			Application.known_nodes.remove(this.id);
		} catch (IOException e) {
			Logger.log(Constants.NETWORK + "Connection with node: " + this.getId() + " was closed", Logger.Level.ERROR);
			result = false;
		}
		return result;
	}

	public boolean isConnected() {
		if (connection == null) {
			return false;
		} else {
			return connection.isConnected();
		}
	}

	/* Getters and setters */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String toString() {
		return address + ":" + port;
	}

	public double getRandomSeed() {
		return this.random_seed;
	}
}
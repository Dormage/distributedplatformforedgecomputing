package influxDB;

import com.google.gson.Gson;
import common.*;
import configs.NetworkConfig;
import helpers.Constants;
import helpers.Logger;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.Pong;
import org.influxdb.dto.Query;

import java.util.concurrent.TimeUnit;

public class Influx {
    InfluxDB influxDB;
    String prefix="";

    public Influx(String url, String username, String password) {
        influxDB = InfluxDBFactory.connect(url, username, password);
        Pong response = this.influxDB.ping();
        if (response.getVersion().equalsIgnoreCase("unknown")) {
            Logger.log(Constants.ERROR+ "Error pinging influx DB server.", Logger.Level.ERROR);
            return;
        }else{
            initDatabase();
        }
        this.prefix = prefix;
    }

    public void initDatabase(){
        Logger.log("Init DB", Logger.Level.INFO);
        influxDB.query(new Query("CREATE DATABASE PROD"));
    }
    public void newBLockAccepted (Block block){
        influxDB.setDatabase(prefix+"CONSENSUS");
        influxDB.write(Point.measurement("new_block")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("node_id", NetworkConfig.node_id)
                .addField("height", block.getId())
                .addField("block_producer", block.getBlock_producer())
                .addField("block_hash", block.getBlock_hash())
                .addField("previous_block_hash", block.getPrevious_block_hash())
                .addField("ticket", block.getTicket())
                .addField("VDF_proof", block.getVdf_proof())
                .addField("difficulty", block.getDifficulty())
                .build());
    }
    public void newBlockForged (Block block){
        influxDB.setDatabase(prefix+"CONSENSUS");
        influxDB.write(Point.measurement("forged_block")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("node_id", NetworkConfig.node_id)
                .addField("height", block.getId())
                .addField("block_producer", block.getBlock_producer())
                .addField("block_hash", block.getBlock_hash())
                .addField("previous_block_hash", block.getPrevious_block_hash())
                .addField("ticket", block.getTicket())
                .addField("VDF_proof", block.getVdf_proof())
                .addField("difficulty", block.getDifficulty())
                .build());
    }
    public void newTicket (Block block, String proof, Long duration, Long start_time){
        influxDB.setDatabase(prefix+"CONSENSUS");
        influxDB.write(Point.measurement("ticket")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("node_id", NetworkConfig.node_id)
                .addField("height", block.getId())
                .addField("block_hash", block.getBlock_hash())
                .addField("proof", proof)
                .addField("duration", duration)
                .addField("start_time", start_time)
                .build());
    }

    public void newPeer (Node node){
        influxDB.setDatabase(prefix+"CONSENSUS");
        influxDB.write(Point.measurement("connection")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("node_id", node.getId())
                .addField("address", node.getAddress())
                .addField("port", node.getPort())
                .build());
    }

    public void newMessage (String origin, String destination, String protocol, String hash, int size){
        influxDB.setDatabase(prefix+"CONSENSUS");
        influxDB.write(Point.measurement("message")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("origin", origin)
                .addField("destination", destination)
                .addField("protocol", protocol)
                .addField("hash", hash)
                .addField("size", size)
                .build());
    }

    public void newMigration (App app, Stat stat){
        influxDB.setDatabase(prefix+"CONSENSUS");
        influxDB.write(Point.measurement("migration")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("app_id", app.getId())
                .addField("docker_id", app.getDid())
                .addField("app_name", app.getName())
                .addField("stat_cpu", stat.getCpu())
                .addField("stat_ram", stat.getRam())
                .addField("stat_disk", stat.getDisk())
                .addField("stat_timestamp", stat.getTimestamp())
                .addField("node_id", stat.getNode_id())
                .build());
    }

    public void fork_detected (Block block){
        influxDB.setDatabase(prefix+"CONSENSUS");
        influxDB.write(Point.measurement("fork")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("node_id", NetworkConfig.node_id)
                .addField("height", block.getId())
                .addField("block_producer", block.getBlock_producer())
                .addField("block_hash", block.getBlock_hash())
                .addField("previous_block_hash", block.getPrevious_block_hash())
                .addField("ticket", block.getTicket())
                .addField("VDF_proof", block.getVdf_proof())
                .addField("difficulty", block.getDifficulty())
                .build());
    }

    public void newTransfer (FileDetails fileDetails, Long duration){
        influxDB.setDatabase(prefix+"CONSENSUS");
        influxDB.write(Point.measurement("transfer")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("name", fileDetails.getName())
                .addField("container_id", fileDetails.getContainer_id())
                .addField("size", fileDetails.getSize())
                .addField("duration", duration)
                .addField("sender_id", fileDetails.getSenderID())
                .addField("receiver_id", fileDetails.getReceiverID())
                .addField("sender_cpu", fileDetails.getSenderCPU())
                .addField("receiver_cpu", fileDetails.getReceiverCPU())
                .addField("block_id", fileDetails.getBlockID())
                .addField("block_producer", fileDetails.getBlockProducer())
                .build());
    }

    public void newNodeResources(int blockID, String nodeID, double cpu){
        influxDB.setDatabase(prefix+"CONSENSUS");
        influxDB.write(Point.measurement("resource_pool")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("block_id", blockID)
                .addField("node_id", nodeID)
                .addField("cpu", cpu)
                .build());
    }

    public void newSensorData(String json){
        Gson gson = new Gson();
        SensorData sensorData = gson.fromJson(json, SensorData.class);
        influxDB.setDatabase(prefix+"CONSENSUS");
        influxDB.write(Point.measurement("sensor_data")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("sensor", sensorData.getMeta().get("sn"))
                .addField("fw", sensorData.getMeta().get("fw"))
                .addField("ip", sensorData.getMeta().get("ip"))
                .addField("name", sensorData.getMeta().get("name"))
                .addField("description", sensorData.getMeta().get("desc"))
                .addField("uptime", sensorData.getMeta().get("uptime"))
                .addField("T", sensorData.getT())
                .addField("RH", sensorData.getRH())
                .addField("CO2", sensorData.getCO2())
                .addField("p", sensorData.getP())
                .addField("ambient_light", sensorData.getAmbient_light())
                .addField("VOC_index", sensorData.getVOC_index())
                .addField("VOC_equiv_CO2", sensorData.getVOC_equiv_CO2())
                .addField("VOC_accuracy", sensorData.getVOC_accuracy())
                .addField("PM2_5", sensorData.getPM2_5())
                .addField("PM10", sensorData.getPM10())
                .build());
    }

    public void newPossibleFork(int blockID, String producer, int ticket){
        influxDB.setDatabase(prefix+"CONSENSUS");
        influxDB.write(Point.measurement("possible_fork")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("block_id", blockID)
                .addField("producer", producer)
                .addField("ticket", ticket)
                .build());
    }

}



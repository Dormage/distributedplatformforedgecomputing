# Decentralized architecture for Edge computing



## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Maven
* Docker


### Installing


*  Clone the entire repository
*  Use the Eclipse to import an existing project to workspace and point it at the root of the repository
*  Build with maven or import libraries as external Jars.


### Building

*  Export the project as a runnable jar to the docker folder (i.e. Node.jar)
*  Cd to the docker folder and build a docker image: `docker build --tag node .`

## Running a node

Start as many terminal windows and run `docker run -it -v /var/run/docker.sock:/var/run/docker.sock -v /usr/bin/docker:/usr/bin/docker node`
The option mounts the unix socket from the host to the container allowing the node to connect to the host's docker daemon instance.
Unfortunatly this will vary depending on the platform.
As an alternative, docker can be configured to expose a web socket instead of unix socket, which is supported by docer-client java library but requires changing the connector in source.
Additionally, a docker volume can be mounted `source=/log` where nodes will create logs for statistics in csv format. (i.e. `docker run -it --mount source=log,target=/log -v /var/run/docker.sock:/var/run/docker.sock -v /usr/bin/docker:/usr/bin/docker node`)



## Dependancies

* [GSON](https://github.com/google/gson) - Google's json library for Java
* [Docker](https://www.docker.com/) - Container framework
* [Docker-client](https://github.com/spotify/docker-client) - Container framework
* [Protocol-buffers](https://developers.google.com/protocol-buffers/) - Google's library for serializing data structures


## Authors

* **Aleksandar Tošič** - *Initial work* - [Dormage](https://gitlab.com/Dormage)
* **Benjamin Božič** - [Ethirallan](https://gitlab.com/Ethirallan)
* **dr. Michael Mrissa** 
* **dr. Jernej Vičič** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments



